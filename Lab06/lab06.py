"""
@file lab06_main.py
@brief Simulation or Reality?
@details This script linearizes the equations found in Lab05 and implements
the model to show the dynamic response of the system in different conditions


@author Hunter Morse
@author Jacob Everest
@date Feb 24, 2021
"""

import numpy as np
import sympy as sp
import matplotlib as mp
from sympy import Matrix
from sympy import sin as sin
from sympy import cos as cos

# Initialize symbols
rm = sp.symbols('rm')
rb = sp.symbols('rb')
rg = sp.symbols('rg')
rp = sp.symbols('rp')
rc = sp.symbols('rc')
lr = sp.symbols('lr')
lp = sp.symbols('lp')
mb = sp.symbols('mb')
mp = sp.symbols('mp')
Ip = sp.symbols('Ip')
Ib = sp.symbols('Ib')
g  = sp.symbols('g')
b  = sp.symbols('b')
Tx = sp.symbols('Tx')
tx = sp.symbols('tx')
ty = sp.symbols('ty')
txd = sp.symbols('txd')
tyd = sp.symbols('tyd')
pos = sp.symbols('pos')
posd = sp.symbols('posd')
w  = sp.symbols('w')
wd = sp.symbols('wd')

# Create state matries
x = Matrix([posd, tyd, pos, ty])	# 4x1
u = Matrix([Tx])

# Init pos
x0 = Matrix([0, 0, 0, 0])
u0 = Matrix([0])

# Create M Matrix
M11 = -(mb*rb^2 + mb*rc*rb + Ib)/rb;
M12 = -(Ib*rb + Ip*rb + mb*rb^3 + mb*rb*rc^2 + 2*mb*rb^2*rc + mp*rb*rg^2 + mb*rb*pos^2)/rb;
M21 = -(mb*rb^2 + Ib)/rb;
M22 = -(mb*rb^3 + mb*rc*rb^2 + Ib*rb)/rb;

M = Matrix([[M11, M12], [M21, M22]])

# Create f Matrix
f1 = b*ty - g*mb * (sin(ty)*(rc+rb)+pos*cos(ty)) + Tx*lp/rm + 2*mb*tyd*pos*posd - g*mp*rg*sin(ty);
f2 = -mb*rb*posd*tyd^2 - g*mb*rb*sin(ty);

f = Matrix([f1, f2])

# Find accelerations
qdd = M.inv()*f

# Calculate Jacobians
Jx = qdd.jacobian(xd, x)
Ju = qdd.jacobian(xd, u)

# Given Values
given_vals = {
	'rm_val' : 0.060		# [m]
	'rb_val' : 0.0105		# [m]
	'rg_val' : 0.042		# [m]
	'rp_val' : 0.0325		# [m]
	'rc_val' : 0.050		# [m]
	'lr_val' : 0.050		# [m]
	'lp_val' : 0.110		# [m]
	'mb_val' : 0.030		# [kg]
	'mp_val' : 0.400		# [kg]
	'Ip_val' : 0.00188		# [kg*m^2]
	'Ib_val' : (2/5)*mb*rb^2# [kg*m^2]
	'g_val'  : 9.81			# [m/s^2]
	'b_val'  : 0.010		# [N*m*s/rad]
}
# Calculate A and B Matricies
vars_to_sub = [rm, rb, rg, rp, rc, lr, lp, mb, mp, Ip, Ib, g, b]
vals_to_sub = list(given_vals.values())

# Sub given vals
if len(vars_to_sub == len(vals_to_sub)):
	A = Jx
	B = Ju
	for i in range(len(vars_to_sub)):
		A = A.subs({vars_to_sub[i]: vals_to_sub[i]})
		B = B.subs({vars_to_sub[i]: vals_to_sub[i]})

# Sub init vals
for i in range(len(x)):
		A = A.subs({x[i]: x0[i]})

for i in range(len(u)):
		B = B.subs({u[i]: u0[i]})


# Plot
time  = [x/10 for x in range(100)]


fig, axs = plt.subplots(2, 2)
y1 = A*x + B*u
axs[0, 0].plot(time, y)
axs[0, 0].set_title('Axis [0,0]')
axs[0, 1].plot(x, y, 'tab:orange')
axs[0, 1].set_title('Axis [0,1]')
axs[1, 0].plot(x, -y, 'tab:green')
axs[1, 0].set_title('Axis [1,0]')
axs[1, 1].plot(x, -y, 'tab:red')
axs[1, 1].set_title('Axis [1,1]')



#Tx_val = 
#tx_val = 
#ty_val = 
#txd_val = 
#tyd_val = 
#pos_val = 
#posd_val = 
#w_val  = 
#wd_val = 
#
#
