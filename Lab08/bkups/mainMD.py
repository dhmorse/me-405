"""
@file motorDriver.py
@brief Motor Driver
@details Driver to interface the DC motors with the STM32 using PWM
and the DRV8847 motor driver

@author Hunter Morse
@date March 11, 2021
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

import micropython

class MotorBase:
	"""
	Balance table motor base
	"""
	def __init__(self, nFault_pin, motor1, motor2):
		"""
		@param nFault_pin fault detection pin 		[IN]
		@param motors list of DC motor objects	
		"""
		self.motor1 = motor1
		self.motor2 = motor2
		self.faultPin = Pin(nFault_pin, mode = Pin.IN)
		self.faultPin = ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)

		self.motors = [self.motor1, self.motor2]

	def faultISR(self, home):
		"""
		Interrupt service routine to handle cases where the nFAULT bit has been 
		tripped. To resume operation the motor must be re-enabled 
		"""
		print("Fault triggered")
		for motor in self.motors:
			motor.stop()
			motor.disable()
		


class DCMotor:
	"""
	DC Motor driver class
	"""

	def __init__(self, nSleep_pin, in1_pin, in2_pin, timer, ch1, ch2, duty=0):
		"""
		@param nSleep_pin enable pin 				[OUT]
		@param nFault_pin fault detection pin 		[IN]
		@param in1_pin motor input pin 1 			[OUT]
		@param in2_pin motor input pin 2			[OUT]
		@param timer timer used for PWM generation	
		@param PWM ch1 channel for in1_pin
		@param PWM ch2 channel for in2_pin
		@param duty default to 0
		"""
		print("Initializing and diabling motor...")
		
		self.enablePin = Pin(nSleep_pin, mode = Pin.OUT_PP)
		# self.faultPin = Pin(nFault_pin, mode = Pin.IN)

		self.mPin1 = in1_pin
		self.mPin2 = in2_pin
		self.ch1 = timer.channel(ch1, Timer.PWM, pin = self.mPin1)
		self.ch2 = timer.channel(ch2, Timer.PWM, pin = self.mPin2)

		self.duty = duty

		# Enable IRQ on faultPin 
		# self.faultPin = ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)
		# self.faultPin.irq(trigger = Pin.IRQ_RISING, handler = self.faultISR)

		print("... motor initialized")
		self.disable()

	def enable(self):
		"""
		Set enablePin (nSleep) to HI to enable motors
		"""
		self.enablePin.high()
		print("Motor enabled")

	def disable(self):
		"""
		Set enablePin (nSleep) to LO to disable motors
		"""
		self.enablePin.low()
		print("... motor diabled")

	#def faultISR(self, home):
	#	"""
	#	Interrupt service routine to handle cases where the nFAULT bit has been 
	#	tripped. To resume operation the motor must be re-enabled 
	#	"""
	#	print("Fault triggered")
	#	self.stop()
	#	self.disable()

	def setDuty(self, duty):
		"""
		Set duty cycle of the motor. Positive values increase effort in one
		direction, negative increase effort in the other.
		"""

		# Ensure |duty| <= 100
		if(duty != 100 and duty != -100):
			duty %= 100		

		# Check to see if duty alrady achieved 
		if(duty == self.duty):
			print("Currently running at %04d" %duty)
			return
		else:
			# If new duty diff. then reset both channels and save new duty
			self.duty = duty
			self.ch1.pulse_width_percent(0)
			self.ch2.pulse_width_percent(0)
			print("Duty reset")

		# [+]duty for ch1, [-]duty for ch2
		if(duty > 0):
			self.ch1.pulse_width_percent(duty)
		elif(duty < 0):
			self.ch2.pulse_width_percent(duty * -1)	# convert [-]duty to [+]duty

		print("Duty set to %04d" %duty)

	def driveFWD(self):
		"""
		Set duty on ch1 to drive motor forward
		"""
		duty = 50
		self.setDuty(duty)
		print("Driving forward")

	def driveBKWD(self):
		"""
		Set duty on ch2 to drive motor forward
		"""
		duty = -50
		self.setDuty(duty)
		print("Driving backward")

	def brake(self):
		"""
		Set duty on both motor channels to stop the motor
		"""
		duty = 50
		self.ch1.pulse_width_percent(duty)
		self.ch2.pulse_width_percent(duty)
		print("Braking")

	def stop(self):
		"""
		Set duty on both motor channels to 0 so motor can coast
		"""
		duty = 0
		self.ch1.pulse_width_percent(duty)
		self.ch2.pulse_width_percent(duty)
		print("Motors stopped")



#def mainLoop(motors):
#	"""
#	"""
#	for motor in motorsa		


def main():
	"""
	"""
	# Constants:
	freq = 5000		# [Hz]

	# Fn pin objects
	nSleep_pin = Pin.cpu.A15
	nFault_pin = Pin.cpu.B2
	
	# Motor 1
	m1in1_pin = Pin.cpu.B4
	m1in2_pin = Pin.cpu.B5
	m1ch1 = 1
	m1ch2 = 2

	# Motor 2 pins
	m2in1_pin = Pin.cpu.B0
	m2in2_pin = Pin.cpu.B1
	m2ch1 = 3
	m2ch2 = 4

	# Create Timer
	tim3 = Timer(3, freq=freq)

	# Create motor objects
	motor1 = DCMotor(nSleep_pin, m1in1_pin, m1in2_pin, tim3, m1ch1, m1ch2)
	motor2 = DCMotor(nSleep_pin, m2in1_pin, m2in2_pin, tim3, m2ch1, m2ch2)

	base = MotorBase(nFault_pin, motor1, motor2)

	motor1.enable()
	motor1.driveFWD()
	motor2.enable()
	motor2.driveFWD()
	utime.sleep(10)
	print("out")
	motor1.disable()
	motor2.disable()



if __name__ == '__main__':
	main()


"""
notes:
	nFAULT bit only occurs when both motors are held
	oscillating behavior
"""