"""
@file lab04.py
Documentation of lab04.py

@author Hunter Morse
@author Jacob Everest
@date Feb 11, 2021
"""

import pyb
from pyb import ADCAll
from utime import sleep

def getInternalTemp(adcall):
	temp = adcall.read_core_temp();
	return temp


def main():
	"""
	Main
	"""
	# Create an ADCALL objetct, temp on channel 16
	adcall0 = ADCAll(12, 0x70000)

	# Create buffer as array for quick ADC values. 
	# Note: not using bytearray becase all 12 bits are desired 
	buff = array('H', (0 for index in range(1000)))		# 'H' is unsigned short


	with open("temps.csv", "w") as temp_file:
		temp = getInternalTemp(adcall0)
		temp_file.write("%0.3f\r\n" %temp)
		sleep(60)

if __name__ == '__main__':
	main()



