"""
@file lab04.py
Documentation of lab04.py

@author Hunter Morse
@date Feb 11, 2021
"""


"""
Micro controller POV:
I2C: 	-->	Start Condition
			7-bit address of device with which it will exchange data
		--> Bit 8 indicates r/w
		
		Now that address is established need to know where to put data
		-->	Register address transfered

		~ Data exchanged ~

Our goals:
1. Write a script for the micro controller to measure its internal temp
2. Write a module, mcp9808.py, containing a class with the following methods:
	- An initializer
"""

import pyb
from pyb import ADCALL
from utime import sleep

def getInternalTemp():



def main():
	"""
	Main
	"""
	# Create an ADCALL objetct, temp on channel 16
	adcall0 = ADCALL(12, 0x70000)

	# Create buffer as array for quick ADC values. 
	# Note: not using bytearray becase all 12 bits are desired 
	buff = array('H', (0 for index in range(1000)))		# 'H' is unsigned short


	with open("temps.csv", "w") as temp_file:
		temp = getInternalTemp(adcall0)
		temp_file.write("%0.3f\r\n" %temp)
		sleep



