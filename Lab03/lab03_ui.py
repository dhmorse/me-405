"""
@file lab03_ui.py
Documentation of lab03_ui.py

@author Hunter Morse
@date Feb 04, 2021
"""
import serial
import time
from matplotlib import pyplot


#class BoardCom:
#    """
#    Object class for easy serial commuination with an STM32 board running micropython
#    """
#    def __init__(self, comm):
#        """
#        @param comm
#        """

def handshake(ser, payload):
    """
    Write to Nucleo through Serial. Return [T/F] if handshake recieved/not.
    @param ser is the open serial communication port
    @param payload is the data to send over ser
    """
    #ser.write((str(payload)+"\r\n").encode('ascii'))         # Send to board
    ser.write(str(payload).encode('ascii'))
    time.sleep(0.01)                                 # Pause
    ret = str(ser.readline().decode('ascii')).strip() # Recieve handshake
    print("To board: %s     From board: %s" %(str(payload), ret))

    if(payload == ret):
        return ret      # return the value recieved from the nuc

def startUI(key):
    """
    UI Start message. This message welcomes the user and instructs them
    to enter the deired key. The function continues to ask for the 
    desired key until it's submitted or the program exits. Upon recieving
    the desired key the program performs a handshake with the board 
    @param key is the desired key
    """
    inKey = 'z'

    print("Hello! Please enter '%s' to begin." %key)
    while inKey.lower() != key:
        inKey = input("Enter '%s': " %key)

    #if(inKey.lower() == 'g'):
        # If 'g' then send signal to board
        # hi ~4090-4095

def plotData(x, y):
    """
    Processes and plots data using the matplot lib library
    @param data is the data to plot
    """
    y = [val*3.3/4095 for val in y]
    #for val in range(len(y)):
        # point = str(line, 'ascii').split(',')
        # tout.append(int(point[0])*10)           # x10 for microseconds
        # vout.append(int(point[1])*3.3/4096)     # Voltage [V]

    pyplot.plot(x, y, "r-")
    pyplot.xlabel("Time [microsecond]")
    pyplot.ylabel("Voltage [V]")
    pyplot.title("Voltage Change From Button Release")
    pyplot.show()



def main():
    """
    Main function of Lab 03 UI. Establishes coms connections and delegates
    """

    key = 'g'

    # Establish serial conncetion with board
    ser = serial.Serial(port = '/dev/tty.usbmodem205F316230562', baudrate = 115273, timeout = 1)
    begin = False
    data = None
    times = []
    voltages = []

    # wait for g to be pressed and handshake with board to occur
    startUI(key)

    # Wait for handshake with board
    while(begin is False):    
        begin = (handshake(ser, key) == key)

    # Once connected to board wait for data to transfer
    while(data != 'start'):
        print('waiting for start')
        data = ser.readline().decode('ascii').strip()
        print(data)

    data = ser.readline().decode('ascii').strip()

    while(data != 'end'):
        dps = data.split(',')           # data points
        times.append(int(dps[0]))       # time is first of dps
        voltages.append(int(dps[1]))    # voltage is second of dps
        data = ser.readline().decode('ascii').strip()

    #while not data:
    #    try:
    #        data = serial.readline().decode('ascii').strip()
        #except:
        #    print('Port not oppened properly')
        #    ser.close()
        #    return

    # Make CSV
    with open('lab03_results.csv', 'w') as file:
        for n in range(len(times)):
            line = ("%04d, %.04f\n" %(int(times[n]), float(voltages[n])))
            file.write(line)

    # Print times and voltages
    print("times:", end = '')
    print(times)
    print("voltages:", end = '')
    print(voltages)


    # Plot data
    plotData(times, voltages)

    # Close serial connection
    ser.close()

if __name__ == '__main__':
    main()