"""
@file main.py
@brief 		Pushing the Right Buttons
@details 	Pressing the user button on the Nucleo (default HI) cuases
			the voltage to drop before returning to its HI state. This
			script sends data collected from the button press as a CSV
			to a host computer over a UART Serial conncetion

@author Hunter Morse
@date Feb 04, 2021
"""

from array import array as arr

import pyb
from pyb import Pin, Timer, USB_VCP, ADC, ExtInt

import micropython



class ADCTestObj:
	"""
	@brief 		Button Press Object
	@details	Store values and functions useful for keeping track of 
				interupts and ADC data
	"""
	def __init__(self, pin, btn, led, timer, bus, buff):
		"""
		@param pin ADC read pin
		@param btn button response to be read by ADC
		@param led LED for visual feedback on code execution
		@param timer timer to read off ADC values
		@param bus UART data bus to send/recieve data
		@param buff buffer size to store data for sending
		"""
		self.ADC = ADC(pin)
		ExtInt(btn, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.btnPressed)
		self.LED = Pin(led, mode=pyb.Pin.OUT_PP)
		self.timer = timer
		self.bus = bus
		self.buff = arr('H', (0 for index in range(buff)))

		self.btnPressed_flag = 0	# [1/0] flag indicating button pressed/not pressed
		self.handshake_flag = 0		# [1/0] flag indicating handshake occured
		self.LED.off()

	def handshake(self):
		"""
		@brief		Handshake with host computer
		@details	Waits for incoming value on UART and returns that value
		@return busVal is the value recieved over UART from the host computer
		"""
		busVal = None
		while busVal is None:
			# if signal comes thru return that value
			if(self.bus.any()):
				busVal = self.bus.read()
				self.bus.write(busVal)
				self.handshake_flag = 1		# Handshake complete - interrupt can occur
			else:
				# Keep waiting
				pass

		return busVal

	def btnPressed(self, home):
		"""
		@brief		Button Press ISR
		@details	ISR occuring on btn press. Sets btnPressed_flag and 
					only triggers if handshake complete
		@param home required ISR return value
		"""
		# Only run ISR if handshake has occured 
		if (self.handshake_flag == 1):
			self.LED.off()				# turn off board LED
			self.btnPressed_flag = 1	# raise btnPressed_flag		

	def readADC(self):
		"""
		@brief 		Read button press voltage
		@details	Read ADC values at rate specified by timer and save to buffer
		"""
		self.ADC.read_timed(self.buff, self.timer)

	def writeOutADC(self):
		"""
		@brief		Export data to computer over UART
		@details	Convert ADC values in buffer to voltages and write out to comm port.
		"""
		self.btnPressed_flag = 0
		self.bus.write('start\n'.encode('ascii'))
		for n in range(len(self.buff)):
			if(self.buff[n] < 4095 and self.buff[n] > 10):
				time = (n * 50)						# Time in us
				self.bus.write('{:}, {:}\r\n'.format(time, self.buff[n]))
			
		self.bus.write('end\n'.encode('ascii'))
		self.readADC()
		

		

def mainLoop(obj):
	"""
	@brief		Main Loop
	@details	Main loop used to executed the primary function loop run on the nucleo
	@param obj An ADCTestObj
	"""

	# Wait for connection to be established between Nucleo and Computer
	while(True):
		obj.handshake()
		obj.LED.on()						# Turn on board LED
	
		# Wait for button to be pressed
		while(not obj.btnPressed_flag):
			pass
		
		obj.readADC()
		obj.writeOutADC()


def main():
	"""
	@brief		Main
	@details	Used to initialize the necessary pins, ports, and vars
				then initiates the main loop
	"""
	# Allocate memory for ISR debug messages
	micropython.alloc_emergency_exception_buf(100)
	
	# Create timer
	tim5 = Timer(5, freq=100000)   # Micro second timer

	# Pin for ADC
	pin0 = Pin.board.PA0    

	# Btn for interrupts
	btn13 = Pin.board.PC13

	# LED for visual indictions
	led5 = Pin.board.PA5

	# Define buffer size
	buffSize = 500

	# Establish serial communication bus
	uart = USB_VCP()

	# Create ADCTestObj object
	adcObj = ADCTestObj(pin0, btn13, led5, tim5, uart, buffSize)

	# Branch to main loop now that everything has been initialized
	mainLoop(adcObj)


if __name__ == '__main__':
	main()
