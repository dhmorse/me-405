"""
@file lab03.py
Documentation of lab03.py

@author Hunter Morse
@date Feb 04, 2021
"""
# GOAL: Build UI to examine and visualize how input voltage changes
# 		as a function of time

# 'G' key in console to command Nucleo to await user input
# Once btn pressed, capture input voltage (ADC counts) in an array
# Transmit data back to laptop where it will be saved as a CSV
# Plot data using matplotlib

# Nucleo ADC: Vref = 3.3 +/-5%, 12-bit resolution (8-bit for high speed)
# Must use pyb.Pin.board.* where * starts with P

from array import array
import pyb 

def main():
	"""
	Main
	"""
	pin0 = pyb.Pin.board.PA0	# Create Pin object with pin PA0
	adc0 = pyb.ADC(pin0)		# Create ADC object with pin0

	# Read analog value from pin 1:
	adc0_raw = adc0.read()		# Raw adc0 value

	# Convert analog value to voltaga
	adc0_voltage = adc0_raw * (3.3/4096)

	# Create buffer as array for quick ADC values. 
	# Note: not using bytearray becase all 12 bits are desired 
	buff = array('H', (0 for index in range(500)))		# 'H' is unsigned short







class Pin:
	"""
	Used to create pin objects
	"""
	def __init__(
		self,

		)
