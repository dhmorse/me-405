"""
@file lab03_main.py
Documentation of main.py
Backend running on the Nucleo

@author Hunter Morse
@date Feb 04, 2021
"""

from array import array
from pyb import UART
import micropython
import pyb

class ADCTest:
    """
    Store values and functions useful for keeping track of interupts
    and ADC data
    """
    def __init__(
        self,
        thresh_min,
        thresh_max,
        adc,
        buff,
        comm,
        u_LED,
        timer,
        t_start = 0,
        t_end = 0,
        BTN_pressed = False
        ):
        """
        @param thresh_min acceptable min voltage
        @param thresh_max acceptable max voltage
        @param adc analog to digita converter
        @param buff stored ADC values to be returned
        @param comm UART serial communication
        @param u_LED LED to turn on while filling buff
        @param timer timer object
        @param t_start start time
        @param t_end end time
        @param BTN_pressed button interupt flag
        """
        self.thresh_min = thresh_min
        self.thresh_max = thresh_max
        self.adc = adc
        self.buff = buff
        self.comm = comm
        self.u_LED = u_LED
        self.timer = timer
        self.t_start = t_start
        self.t_end = t_end
        self.BTN_pressed = BTN_pressed

    def readADC(self):
        # Read ADC values at rate specified by timer and save to buffer
        self.adc.read_timed(self.buff, self.timer)

    def writeOutADC(self):
        # Write ADC values in buffer to comm port
        for n in range(len(self.buff)):
            self.comm.write('{:}, {:}\r\n'.format(n, self.buff[n]))



def startRecording(tst):
    """
    Starts the timer, turns on the LED, raises the BTN_pressed flag
    @param tst stores values and objects to keep track of a given button test instance
    """
    tst.u_LED.high()                    # Turn on LED
    tst.timer.counter(0)                # Reset counter to 0
    tst.t_start = tst.timer.counter()   # Capture count start
    tst.BTN_pressed = True              # Raise BTN_pressed flag

def endRecording(tst):
    """
    Ends the timer, turns off the LED, lowers the BTN_pressed flag
    @param tst stores values and objects to keep track of a given button test instance
    """
    tst.t_end = tst.timer.counter()     # Capture count end
    tst.u_LED.low()                     # Turn off LED
    tst.BTN_pressed = False             # Lower BTN_pressed flag


def main():
    """
    Main. Used to initialize the necessary pins, ports, and vars
    """
    ## Allocate memory for ISR debug messages
    micropython.alloc_emergency_exception_buf(100)
    # Create timer
    tim2 = pyb.Timer(6, freq=1000000)   # Micro second timer

    # Create Pin object with pin PA0
    pin0 = pyb.Pin.board.PA0    

    # Create ADC object with pin0
    adc0 = pyb.ADC(pin0)        

    # Create buffer as array for quick ADC values. 
    # Note: not using bytearray becase all 12 bits are desired 
    buff = array('H', (0 for index in range(1000)))     # 'H' is unsigned short

    # Enable btn to trigger ext interrupts
    i_BTN = pyb.ExtInt(pyb.Pin.board.PC13, 
        pyb.ExtInt.IRQ_FALLING, 
        pyb.Pin.PULL_NONE, 
        startRecording)

    # Set up green LED for visual indicator of process progressing
    g_LED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)


    # Establish serial communication
    uart = UART(2)

    # Complete handshake
    handshake_complete = False

    while(not handshake_complete):
        if uart.any() != 0:
            val = uart.readchar()
            uart.write('Recieved: %s (ascii)' %str(val))
            handshake_complete = True

    tst = ADCTest(
        thresh_min = 256, 
        thresh_max = 3840, 
        adc = adc,
        buff = buff, 
        comm = uart,
        u_LED = g_LED, 
        timer = tim2)

    while (not tst.BTN_pressed):
        pass


    if tst.BTN_pressed:
        tst.readADC()                   # Read ADC values and save them to buffer
        tst.writeOutBuff()              # Write out ADC results to comm port
        if(tst.comm.readchar()!='Y'):   # Check data was recieved
            print("Error: Unverified transfer")


    uart.deinit()                       # Close UART bus
