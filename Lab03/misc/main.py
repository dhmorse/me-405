"""
@file main.py
Documentation of main.py
Backend running on the Nucleo

@author Hunter Morse
@date Feb 04, 2021
"""

from array import array as arr

import pyb
from pyb import Pin, Timer, USB_VCP, ADC, ExtInt

import micropython



class ADCTestObj:
	"""
	Store values and functions useful for keeping track of interupts
	and ADC data
	"""
	def __init__(self, pin, btn, led, timer, bus, buff):
		"""
		@param pin ADC read pin
		@param btn button response to be read by ADC
		@param led LED for visual feedback on code execution
		@param timer timer to read off ADC values
		@param bus UART data bus to send/recieve data
		@param buff buffer size to store data for sending
		"""
		self.ADC = ADC(pin)
		ExtInt(btn, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.btnPressed)
		self.LED = Pin(led, mode=pyb.Pin.OUT_PP)
		self.timer = timer
		self.bus = bus
		self.buff = arr('H', (0 for index in range(buff)))

		self.btnPressed_flag = 0	# [1/0] flag indicating button pressed/not pressed

		self.LED.off()

	def handshake(self):
		"""
		Waits for incoming value on UART and returns that value
		"""
		busVal = None
		while busVal is None:
			#print(self.bus.any())
			if(self.bus.any()):
				busVal = self.bus.read()
				self.bus.write(busVal)
				#print("Read: %s" %busVal)
			else:
				pass
				#print("...")

		return busVal

	def btnPressed(self, home):
		"""
		ISR occuring on btn press. Sets btnPressed_flag
		# and release. Flips flag from 0 to 1 or 1 to 0
		"""
		self.LED.off()				# turn off board LED
		self.btnPressed_flag = 1	# raise btnPressed_flag		



	#	self.LED.off()				# turn off board LED
	#	if(self.btnPressed_flag == 1):
	#		self.btnPressed_flag = 0
	#	else:
	#		self.btnPressed_flag = 1	# raise btnPressed_flag

	def readADC(self):
		"""
		Read ADC values at rate specified by timer and save to buffer
		"""
		self.ADC.read_timed(self.buff, self.timer)

	def writeOutADC(self):
		"""
		Convert ADC values in buffer to voltages and write out to comm port.
		Reset ADC buffer by reading new values w/o button press
		"""
		#times = []
		#voltages = []
		self.btnPressed_flag = 0
		self.bus.write('start\n'.encode('ascii'))
		for n in range(len(self.buff)):
			if(self.buff[n] < 4095 and self.buff[n] > 10):
				time = (n * 50)						# Time in us
				voltage = self.buff[n] * (3.3/4095)	# ADC value to volts
				self.bus.write('{:}, {:}\r\n'.format(time, self.buff[n]))

			#times.append(n * 50)						# Time in us
			#voltages.append(self.buff[n] * (3.3/4095))	# ADC value to volts
			
		#self.bus.write('{:}, {:}\r\n'.format(times, voltages))
		self.bus.write('end\n'.encode('ascii'))
		

		

def mainLoop(obj):
	"""
	Main loop used to executed the primary function loop run on the nucleo
	@param obj An ADCTestObj
	"""

	# Wait for connection to be established between Nucleo and Computer
	obj.handshake()
	obj.LED.on()						# Turn on board LED

	# Wait for button to be pressed
	while(not obj.btnPressed_flag):
		pass
	
	obj.readADC()
	obj.writeOutADC()

	# Refresh obj buff
	# print("refresh ADC")
	# obj.readADC()



#	while(True):
#		if(not obj.btnPressed_flag):
#			pass
#		else:
#			obj.readADC()
#			obj.writeOutADC()
	




def main():
	"""
	Main. Used to initialize the necessary pins, ports, and vars
	"""
	# Allocate memory for ISR debug messages
	micropython.alloc_emergency_exception_buf(100)
	
	# Create timer
	tim5 = Timer(5, freq=20000)   # Micro second timer

	# Pin for ADC
	pin0 = Pin.board.PA0    

	# Btn for interrupts
	btn13 = Pin.board.PC13

	# LED for visual indictions
	led5 = Pin.board.PA5

	# Define buffer size
	buffSize = 2048

	# Establish serial communication bus
	uart = USB_VCP()

	# Create ADCTestObj object
	adcObj = ADCTestObj(pin0, btn13, led5, tim5, uart, buffSize)

	# Branch to main loop now that everything has been initialized
	mainLoop(adcObj)


"""   
 # Complete handshake
	   handshake_complete = False
   
	   print('Handshake ...')
   
	   while(not handshake_complete):
		hsChar = 
		   if uart.any() != 0:
			   val = uart.readchar()
			   uart.write('Recieved: %s (ascii)\n' %str(val))
			   handshake_complete = True
   
	   print('... complete!')
   
	   tst = ADCTest(
		   thresh_min = 256, 
		   thresh_max = 3840, 
		   adc = adc,
		   buff = buff, 
		   comm = uart,
		   u_LED = g_LED, 
		   timer = tim2)
   
	   while (not tst.BTN_pressed):
		   pass
   
   
	   if tst.BTN_pressed:
		   tst.readADC()                   # Read ADC values and save them to buffer
		   tst.writeOutBuff()              # Write out ADC results to comm port
		   if(tst.comm.readchar()!='Y'):   # Check data was recieved
			   print("Error: Unverified transfer")


	uart.deinit()                       # Close UART bus
"""

if __name__ == '__main__':
	main()
