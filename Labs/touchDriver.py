"""@file touchDriver.py
@brief Placement on touchscreen
@details Initializes a touchy class. Takes readings of where the attached 
         touchscreen is being touched. Returns values for x location, y
         location, and whether or not the screen is even being touched. 
@author Hunter Morse
@date Mar. 1, 2021
@copyright None
"""

import pyb
from pyb import Pin
from pyb import ADC

import utime
from utime import sleep_us
from utime import ticks_us
from utime import ticks_diff

import micropython

class TouchPanel:
    """
    @brief Tells where it is being touched
    @details Input 4 pin values, the length of the touchscreen, the width of
             the touchscreen, the distance from x = 0 edge to the center, and 
             the distance form the y = 0 edge to the center. Once given, the 
             class will return results from whichever method activated. Also
             tells wether or not it is even being touched
    @author Hunter Morse
    @date Mar. 1, 2021
    @copyright None
    """

    def __init__(self, xp, xm, yp, ym, p_len, p_width, center, res = 0xfff, delay = 3):
        """
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param xp pin registered to xp
        @param xm pin registered to xm
        @param yp pin registered to yp
        @param ym pin registered to ym
        @param p_len panel length
        @param p_width panel width
        @param center ox and oy coordinate tuple of panel origin (center)
        @param res resolution of adc (default 0xfff -- 12 bit ADC)
        @param delay read delay to ensure signal settled (delay 3us)
        """

        self.xp = xp
        self.xm = xm
        self.yp = yp
        self.ym = ym

        self.p_len = p_len
        self.p_width = p_width
        self.ox = center[0]
        self.oy = center[1]

        self.res = res
        self.delay = delay		# Read delay default 3. Can be adjusted based on testing

        self.hiPin = None
        self.loPin = None
        self.vPin = None		# Pin to read voltage
        self.fPin = None		# Pin to float


    def xScan(self):
        """ 
        @brief xScan method
        @details Reads the value of the Ym pin to determine the voltage being
                 sent to it by contact with the Xp Xm line. This voltage is 
                 proportional to the distance along the x axis where the screen 
                 is being touched. That is converted from an analog ot a 
                 digital value between 0 and 4095. The value is then divided
                 by 4095 and multiplied by the length of the screen to get the
                 actual x placement of where the screen is being touched. X0 is
                 then subtracted from that value to put the origin at the 
                 center of the screen lengthwise, with the negative being on 
                 the left and the positive being on the right.
        @param None
        """
        #start_time = ticks_us()
        self.hiPin = Pin(self.xp, mode = Pin.OUT_PP).value(1)	# set xp HI
        self.loPin = Pin(self.xm, mode = Pin.OUT_PP).value(0)	# set xm LO
        self.vPin = ADC(self.ym)								# Read voltage from ym
        self.fPin = Pin(self.yp, mode = Pin.IN)					# Float yp

        sleep_us(self.delay)						# wait for signal to settle

        vout = self.vPin.read()						# read voltage from vPin
        x = (vout - self.ox)/self.res * self.p_width 

        
        #end_time = ticks_us()		
        #print("x.time = %d" %(ticks_diff(end_time, start_time)))
        print("x.vout = %04d" %vout)

        return x * 1000		

    def yScan(self):
        """ 
        @brief yScan method
        @details Reads the value of the Xm pin to determine the voltage being
                 sent to it by contact with the Yp Ym line. This voltage is 
                 proportional to the distance along the y axis where the screen 
                 is being touched. That is converted from an analog ot a 
                 digital value between 0 and 4095. The value is then divided
                 by 4095 and multiplied by the width of the screen to get the
                 actual x placement of where the screen is being touched. Y0 is
                 then subtracted from that value to put the origin at the 
                 center of the screen widthwise, with the negative being on 
                 the bottom and the positive being on the top.
        @param None
        """
        self.hiPin = Pin(self.yp, mode = Pin.OUT_PP).value(1)	# set yp HI
        self.loPin = Pin(self.ym, mode = Pin.OUT_PP).value(0)	# set ym LO
        self.vPin = ADC(self.xm)								# Read voltage from xm
        self.fPin = Pin(self.xp, mode = Pin.IN)					# Float xp

        sleep_us(self.delay)						# wait for signal to settle

        vout = self.vPin.read()						# read voltage from vPin
        y = (vout - self.oy)/self.res * self.p_len
        
        #(vout/self.res)*self.p_width - self.oy	# get y position from center
        print("y.vout = %04d" %vout)
        
        return y * 1000

    def zScan(self):
        """ 
        @brief zScan method
        @details Reads the value of the Ym pin to determine the voltage being
                 sent to it by contact with the Yp Xm line. When the screen is
                 being touched, Ym will receive a lower amount of voltage as
                 some of the voltage will go to Xm which is set to low. When
                 the value being read by Ym decreases, Z boolean pin will be
                 set to say that contact has been made.
        @param None
        """
        self.hiPin = Pin(self.yp, mode = Pin.OUT_PP).value(1)	# set yp HI
        self.loPin = Pin(self.xm, mode = Pin.OUT_PP).value(0)	# set xm LO
        self.vPin = ADC(self.ym)								# Read voltage from ym
        self.fPin = Pin(self.xp, mode = Pin.IN)					# Float xp

        sleep_us(self.delay)						# wait for signal to settle

        vout = self.vPin.read()						# read voltage from vPin

        if (100 < vout and vout < 0xf0f):			# check not HI
            print("Touch vout: %04d" %vout)
            return True								# panel touched
        
        print("No touch vout: %04d" %vout)
        return False								# panel not touched

    def scan(self):
        """ 
        @brief scan coordinates
        @details Runs the x coordinate, y coordinate, and z boolean methods. It
                 then returns those values in a tuple. 
        @param None
        """
        return (self.xScan(), self.yScan(), self.zScan())


    def filter(self):
        """
        signal filtering for later implementation
        """
        pass

def makeTouchDriver():
    """
    @brief Touch Driver maker function
    @details Creates a TouchPanel object with default values and uses the
             TouchDriver class
    @param None
    @return panel A TouchPanel Object
    """
    ym = Pin.cpu.A0
    xm = Pin.cpu.A1
    yp = Pin.cpu.A6
    xp = Pin.cpu.A7

    p_len = 0.185		# [m]
    p_width = 0.105		# [m]

    # Determined experimentally by reading x and y ADC values 
    center = (2000, 2000)

    # 12 bit ADC gives a resolution of 0xfff (4095)
    res = 0xfff

    panel = TouchPanel(xm, xp, ym, yp, p_len, p_width, center)
    return panel


def testPanel(panel):
    """
    @brief Test panel function
    @details Tests whether pannel is being touched and how long it takes to work. Implements
             the TouchPanel class to do so.
    @params panel A TouchPanel object
    @return None
    """
    while(True):
        results = panel.scan()
        if(not results[2]):
            # Screen not touched
            print("No touch")
        else:
            # Screen touched
            start_time = ticks_us()
            panel.scan()
            end_time = ticks_us()
            print("x: %04dmm   y: %04dmm   touched: %s   time: %05d" \
              %(results[0], results[1], results[2], end_time-start_time))
        
        # delay before next read. 100000 to give enough time to read when debugging
        sleep_us(1000000)


"""
Bottom	Left : (0380, 3800)
      	Right: (3600, 3800)
Top 	Left : (0420, 0220)
        Right: (3620, 0220)

Center: (2020, 20200)
""" 
