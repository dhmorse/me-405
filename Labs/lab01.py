"""
@file lab01.py
@brief      A reintroduction to Python and finite state machines.
@details    The goal of this lab is to regain comfort with python 
            and the concepts learned in ME 305 by developing a program that 
            "embodies the function of a vending machine."

@author Hunter Morse
@date Jan. 14, 2021
"""
import keyboard

# https://bitbucket.org/spluttflob/me405-support/src/master/ tstkbd clear push key

class Conditions:
    """
    Class to keep track of vending machine conditions to instruct master mind
    """
    def __init__(
        self, 
        state = 0,              ## 
        last_key = '',          ##
        disp_next = '',         ##
        soda_selected = '',     ##
        balance = 0,            ##
        price = 0,              ##
        err_flag = 0,           ##
        disp_flag = 0,          ##
        soda_flag = 0,          ##
        balance_flag = 0,       ##
        change_flag = 0,        ##
        welcome_flag = 0,       ##
        key_pressed_flag = 0,   ##
        wait_for_key_flag = 0,  ##
        reset_flag = 0          ##
        ):

        """
        @param state current state in fsm
        @param last_key last key pressed
        @param disp_next next thing to print to display
        @param soda_selected name of soda selected
        @param balance current balance
        @param price price of selected soda
        @param err_flag [1/0] error raised
        @param disp_flag [1/0] new thing to display
        @param soda_flag [1/0] a soda has been selected
        @param balance_flag [1/0] balance is suffient 
        @param change_flag [1/0] change to be dispensed
        @param welcome_flag [1/0] display welcome message
        @param key_pressed_flag [1/0] a key has been pressed
        @param wait_for_key_flag [1/0] need to wait for key press
        @param reset_flag [1/0] ok to reset conditions
        """

        self.state = state
        self.last_key = last_key
        self.disp_next = disp_next
        self.soda_selected = soda_selected
        self.balance = balance
        self.price = price
        self.err_flag = err_flag
        self.disp_flag = disp_flag
        self.soda_flag = soda_flag
        self.balance_flag = balance_flag
        self.change_flag = change_flag
        self.welcome_flag = welcome_flag
        self.key_pressed_flag = key_pressed_flag
        self.wait_for_key_flag = wait_for_key_flag
        self.reset_flag = reset_flag

    def formatBalance(self):
        """
        @brief      Formats balance
        @details    Format balance from cents to dollar amount string
        """
        dec = self.balance % 100
        dol = self.balance // 100

        return("$%d.%02d" %(dol,dec))

    def formatPrice(self):
        """
        @brief      Formats price
        @details    Format price from cents to dollar amount string
        """
        dec = self.price % 100
        dol = self.price // 100

        return("$%d.%02d" %(dol,dec))


def printWelcome():
    """
    @brief      Prints welcome message
    @details    Prints a Vendotron^TM^ welcome message with beverage prices
    """
    print("\n* * * * * * * * * * * * * * * * * * * * * * * * * ")
    print("WELCOME\nPlease select a beverage:")
    print("[c] Cuke\n[p] Popsi\n[s] Spryte\n[d] Dr. Pupper")
    print("* * * * * * * * * * * * * * * * * * * * * * * * * ")

def state0(c):
    """
    @brief      Perform State 0 operations
    @details    This init state intializes the FSM itself
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    c.last_key = ''
    c.disp_next = ''
    c.soda_selected = ''
    c.balance = 0
    c.price = 0
    c.err_flag = 0
    c.disp_flag = 0
    c.soda_flag = 0
    c.balance_flag = 0
    c.change_flag = 0
    c.welcome_flag = 0
    c.key_pressed_flag = 0
    c.wait_for_key_flag = 1
    c.reset_flag = 0
    
    c.state = 1

def state1(c):
    """
    @brief      Perform State 1 operations
    @details    Displays the startup message and awaits any input from the machine
                before incrementing the state
    @param c A Conditions object used to keep track of the conditions of the vending machine
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print('In State 1')
    if(c.welcome_flag == 0):
        printWelcome()              # Print welcome message
        c.state = 2
    
    c.welcome_flag = 1

def state2(c):
    """
    @brief      Perform State 2 operations
    @details    Master Mind dictates which state operates next based on the flags raised
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 2")
    digits = '01234567'
    if(c.key_pressed_flag):
        c.state = 15
    elif(c.last_key.lower() == 'c'):    
        c.state = 3 # Cuke Handler
    elif(c.last_key.lower() == 'p'):
        c.state = 4 # Popsi Handler
    elif(c.last_key.lower() == 's'):
        c.state = 5 # Spryte Handler
    elif(c.last_key.lower() == 'd'):
        c.state = 6 # Dr.Pupper Handler    
    elif(c.last_key in digits and c.last_key != ''):
        c.state = 7 # Coin Handler    
    elif(c.err_flag):
        c.state = 8 # Error Handler    
    elif(c.disp_flag):
        c.state = 9 # Update Display Handler    
    elif(c.last_key.lower() == 'e'):
        c.state = 10    # Eject Handler    
    elif(c.soda_flag and c.balance_flag):
        c.state = 11    # Dispense Soda Handler    
    elif(c.change_flag):
        c.state = 12    # Change Handler
    elif(c.reset_flag):
        c.state = 13    # Reset Handler
    elif(c.wait_for_key_flag):
        #c.key_pressed_flag == 0 and c.last_key == '' and c.disp_flag == 0 and c.balance_flag == 0):
        c.state = 14

def state3(c):
    """
    @brief      Perform State 3 operations
    @details    The Cuke Handler sets the current soda selection and price.
                If the balance is insufficient an error is raised
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 3")
    c.soda_selected = "Cuke"
    c.soda_flag = 1
    c.price = 100
    #disp_next = "Cuke: $1.00"    
    if(c.balance >= c.price):
        c.balance_flag = 1
    else:
        c.err_flag = 1
    c.last_key = ''    
    c.state = 2     # Reset to MM

def state4(c):
    """
    @brief      Perform State 4 operations
    @details    The Popsi Handler sets the current soda selection and price.
    If the balance is insufficient an error is raised
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 4")
    c.soda_selected = "Popsi"
    c.soda_flag = 1
    c.price = 120
    #disp_next = "Popsi: $1.20"    
    if(c.balance >= c.price):
        c.balance_flag = 1
    else:
        c.err_flag = 1
    c.last_key = ''
    c.state = 2     # Reset to MM

def state5(c):
    """
    @brief      Perform State 5 operations
    @details    The Spryte Handler sets the current soda selection and price.
    If the balance is insufficient an error is raised
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 5")
    c.soda_selected = "Spryte"
    c.soda_flag = 1
    c.price = 85
    #disp_next = "Spryte: $0.85"    
    if(c.balance >= c.price):
        c.balance_flag = 1
    else:
        c.err_flag = 1
    c.last_key = ''    
    c.state = 2     # Reset to MM

def state6(c):
    """
    @brief      Perform State 6 operations
    @details    The Dr. Pupper Handler sets the current soda selection and price.
    If the balance is insufficient an error is raised
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 6")
    c.soda_selected = "Dr. Pupper"
    c.soda_flag = 1
    c.price = 110
    #disp_next = "Dr. Pupper: $1.10"    
    if(c.balance >= c.price):
        c.balance_flag = 1
    else:
        c.err_flag = 1
    c.last_key = ''    
    c.state = 2     # Reset to MM

def state7(c):
    """
    @brief      Perform State 7 operations
    @details    Update the balance besed on the coin inserted. Raise a flag to
    update the display to reflect the new balance
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 7")
    #print("last key: " + c.last_key)
    if(c.last_key == '0'):
        c.balance += 1
    elif(c.last_key == '1'):
        c.balance += 5
    elif(c.last_key == '2'):
        c.balance += 10
    elif(c.last_key == '3'):
        c.balance += 25
    elif(c.last_key == '4'):
        c.balance += 100
    elif(c.last_key == '5'):
        c.balance += 500
    elif(c.last_key == '6'):
        c.balance += 1000
    elif(c.last_key == '7'):
        c.balance += 2000    
    if(c.soda_flag == 0 or (c.balance < c.price)):
        c.wait_for_key_flag = 1
    elif(c.soda_flag == 1 and (c.balance >= c.price)):
        c.balance_flag = 1
    c.disp_flag = 1 # update balance on display 
    c.last_key = ''
    c.state = 2     # Reset to MM

def state8(c):
    """
    @brief      Perform State 8 operations
    @details    If error flag is raised it's because there's an insufficient balance
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 8")
    c.disp_next = "Insufficient Funds for %s: %s/%s"  %(c.soda_selected, c.formatBalance(), c.formatPrice())
    c.disp_flag = 1 
    c.err_flag = 0    
    c.state = 2     # Reset to MM

def state9(c):
    """
    @brief      Perform State 9 operations
    @details    Update the display to the message stored in disp_next
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 9")
    if(c.disp_flag):
        print("Current Balance: " + c.formatBalance())
        print(c.disp_next)
    else:
        print("ERR: disp_flag not raised")
    if(c.soda_flag == 0 or (c.balance < c.price)):
        c.wait_for_key_flag = 1
    elif(c.soda_flag == 1 and (c.balance >= c.price)):
        c.balance_flag = 1
    c.disp_next = ""
    c.disp_flag = 0
    c.state = 2

def state10(c):
    """
    @brief      Perform State 10 operations
    @details    Raise return money flag so that money is returned on the next MM loop
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 10")
    c.last_key = ''
    c.change_flag = 1
    c.state = 2

def state11(c):
    """
    @brief      Perform State 11 operations
    @details    Dispense soda and recalculate current balance
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 11")
    print("\n* Dispenses %s *\n" %c.soda_selected)
    c.disp_next = "Enjoy your %s!" %c.soda_selected
    c.balance -= c.price    
    c.disp_flag = 1
    c.soda_flag = 0
    c.change_flag = 1    
    c.state = 2

def state12(c):
    """
    @brief      Perform State 12 operations
    @details    Return change and reset to State 1
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    # reset all values to be safe
    #print("In State 12")
    c.disp_next = "Change back: %s" %c.formatBalance()
    c.disp_flag = 1
    c.reset_flag = 1
    c.change_flag = 0
    c.state = 2

def state13(c):
    """
    @brief      Perform State 13 operations
    @details    Reset flags and return to state 1
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    c.reset_flag = 0
    c.balance = 0
    c.change_flag = 0
    c.soda_selected = ""            # a little sus putting this here but needed bc state 11 doesn't print
    c.last_key = ''
    c.disp_next = ''
    c.price = 0
    c.err_flag = 0      # [1/0] Indicate error status
    c.disp_flag = 0     # [1/0] Indicate need to updated display
    c.soda_flag = 0     # [1/0] Indicate soda selection status
    c.balance_flag = 0  # [1/0] Indicate balance suffienet to pay
    c.welcome_flag = 0
    c.wait_for_key_flag = 1
    c.state  = 1

def state14(c):
    """
    @brief      Perform State 14 operations
    @details    Wait for key to be pressed
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    # print("In State 13")
    # Drinks
    keys = 'cpsde01234567'
    for key in keys:
        if(keyboard.is_pressed(key)):
            c.key_pressed_flag = 1
            c.last_key = key
            c.wait_for_key_flag = 0
    c.state = 2

def state15(c):
    """
    @brief      Perform State 15 operations
    @details    Wait for key to be released
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    #print("In State 15")            
    keys = 'cpsde01234567'
    pressed = False
    for key in keys:
        if(keyboard.is_pressed(key)):
            # print("pressed true: " + key)
            pressed = True
    if(pressed is False):
        c.key_pressed_flag = 0
        print("")
        if((c.soda_flag == 0 or c.balance_flag == 0) and c.last_key == ""):
            c.wait_for_key_flag = 1            
    c.state = 2     # if still pressed then will come back to this state



def main_loop(c):
    """
    @brief      FSM Main loop
    @details    Implement the FSM using a while loop and an if statement. Will run
                eternallly intil the user presses CTRL-C
    @param c A Conditions object used to keep track of the conditions of the vending machine
    """
    while True:
    
        if(c.state == 0):   # INIT
            state0(c)
        elif(c.state == 1): # Display Start-up Message
            state1(c)
        elif(c.state == 2): # Master Mind
            state2(c)    
        elif(c.state == 3): # Cuke Handler
            state3(c)
        elif(c.state == 4): # Popsi Handler
            state4(c)
        elif(c.state == 5): # Spryte Handler
            state5(c)
        elif(c.state == 6): # Dr Pupper Handler
            state6(c)
        elif(c.state == 7): # Update balance
            state7(c)
        elif(c.state == 8): # Error Handler
            state8(c)
        elif(c.state == 9): # Update Display
            state9(c)
        elif(c.state == 10):# Return Money Flag
            state10(c)
        elif(c.state == 11):# Dispense Soda
            state11(c)
        elif(c.state == 12):# Return Change
            state12(c)
        elif(c.state == 13):# Reset
            state13(c)
        elif(c.state == 14):# Wait for Key Press
            state14(c)
        elif(c.state == 15):# Wait for Key Release
            state15(c)

def main():
    """
    @brief      Main
    @details    Initialize FSM 
    """

    c = Conditions()
    main_loop(c)

if __name__ == '__main__':
    main()













