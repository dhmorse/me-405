# -*- coding: utf-8 -*-
"""@file mainWithTouchPad.py
@brief Attempt to balance the table with Touchscreen functionality
@details This is essentially the same as mainWithnFault.py just with a bit of 
         the touch screen added in. We started by adding the touch screen to 
         shut off the motors entirely. This was easy, just examine the list
         sent by the touchy.py class and if the zboolean was 1, disable the 
         motors. That worked like a charm. Then we tried to set the x motor to
         run and push up when the screen was touched to offset the distance and
         roll the ball back to center. This kind of worked but in combination
         with the past proportional control and some broken touch screen wiring
         it actually became very difficult. I should mention that I worked on 
         this between mainWithnFault and mainThatBalancedtheTable when I needed
         a break from trying to balance the table. 
@author Jacob Everest
@author Hunter Morse
@date Mar. 15, 2021
@copyright This document shall only be viewed by those who will give us an A
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver
import touchy2
from touchy2 import touchy
##@brief Touchy pin
# @details Touchy object to read where the touch screen is being touch
touch = touchy(pyb.Pin.board.PA1, pyb.Pin.board.PA7, pyb.Pin.board.PA0, pyb.Pin.board.PA6, 180, 90, 90, 45)
##@brief Flag for faulting
# @details when high, there has been a fault, when low, it's been resolved 
faultflag = 0 #flag to be triggered when a fault occurs
##@brief Fault pin
# @details Pin that the faults are dependent upon

def fault_isr (fault): #fault isr to be triggered when bad juju happens
    """
    @brief Fault ISR
    @details Sets the faultflag to one if a fault isr has been triggered. Stops
             all functionality until resolved.
    @author Jacob Everest
    @author Hunter Morse
    @date Mar. 15, 2021
    @copyright No
    """
    global faultflag #use fault flag in here
    faultflag = 1 #set flag to 1 to indicate a fault has occured    
##@brief External Interrupt Object
# @details Allows the Fault interrupt to be called
extint = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fault_isr)
#external interrup to be used when current draw to PB2 is too high

##@brief Fix pin
# @Pin to be pressed that would fix the faults
fix = pyb.Pin.board.PC13 #"fix" pin, all this stuff isn't working though
def fix_isr (fix): #not working
    """
    @brief Fix ISR
    @details Was meant to allow the user to press the blue button on the 
             microcontroller to fix the fault interrupt. Didnt end up working
    @author Jacob Everest
    @author Hunter Morse
    @date Mar. 15, 2021
    @copyright No
    """
    global faultflag #not working
    faultflag = 0 #not working  
##@brief External Interrupt Object
# @details Allows the Fix interrupt to be called
fixtint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fix_isr)
#not working
##@brief X axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its widthwise axis
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
##@brief Y axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its lengthwise axis
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#create MotorDriver object
##@brief X axis Motor encoder
# @details Encoder object that outputs position of the X axis motor
balancer = EncoderDriver(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 1, 2, pyb.Timer(4, prescaler = 7, period = 0x7fffffff))
balancer.setPosition(0)
##@brief Y axis Motor encoder
# @details Encoder object that outputs position of the Y axis motor
balancer2 = EncoderDriver(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 1, 2, pyb.Timer(8, prescaler = 7, period = 0x7fffffff))
balancer2.setPosition(0)
##@brief State flag
# @details What else do you want? It's the state flag
state = 0 #use state indicator in loop later
faultflag = 0 #start out making sure fault flag is zero
##@brief Fault ignore
# @details Set number of cycles for which we will ignore the fault flag in order
#          to give the motors time to get running before a real fault
start = 300 #enabling the motor caused a fault in my system, so I created this
#to let the loop below run 300 so it can at least get started
##@brief ADC object
# @details allowed us to connect PC13 to A0 and use the blue button to end the 
#          fault state
adc = pyb.ADC(pyb.Pin.board.A0) #create ADC object so that pin A0 can read the
#value coming from the blue button on the board
##@brief Duty cycle value
# @details Percent of time X motor is receiving energy from PWM
duty = 0
##@brief Duty cycle value
# @details Percent of time Y motor is receiving energy from PWM
duty2 = 0
var.set_duty(0)
var2.set_duty(0)
var.enable()
##@brief New Duty cycle value
# @details Value used for duty cycle when proportionality became unwieldy
dut = 75
##@brief Distance to go
# @details Value computed later that represents how far we are from where we 
#          need to be for balance around the x axis
balanceval = 0
##@brief Distance to go
# @details Value computed later that represents how far we are from where we 
#          need to be for balance around the y axis
balanceval2 = 0
while True: #loop!
    onoff = touch.allread()
    if onoff[2] == 0:
        if start > 0: #if motor is just starting, set faultflag to zero to allow it
        #time to get up to speed
            faultflag = 0 #set flag to zero to ignore the weird glitch
            if start == 300:
                var.enable()
                var2.enable()
                start = start - 1
            elif start == 0: #once start reaches 0, keep it there
                start = 0 #keep start equal to 0 so it doesn't try running back through
            else: #if it hasn't given the motors a chance
                var.enable()
                start = start - 1 #decrement the start counter
        else: #if start is equal to zero, now allow the fault flag to affect the system
            pass
        if faultflag == 0: #if fault flag is not on, run normally
            if state == 0: # if motor is just starting
                var.enable()
                balanceval = balancer.balanceDif()
                if -32768 < balanceval < -5:
                    var.set_duty(-duty)
                elif -5 < balanceval < 5:
                    var.stop()
                else: 
                    var.set_duty(dut)
                balanceval2 = balancer2.balanceDif()
                if -32768 < balanceval2 < -5:
                    var2.set_duty(-dut)
                elif -5 < balanceval2 < 5:
                    var2.stop()
                else: 
                    var2.set_duty(dut)
                #state = 1 #set the state to 1 so it doesnt keep re-enabling the motor
            # if state > 0: #not just starting anymore
            #     pass #run
                
        else: #if fault flag has been set come here
            var.stop() #stop the motors
            var2.stop() #stop the motors
            var.disable() #disable the motors
            state = 0 #set the state back to zero so that when it starts again,
            #we re-enable the motors and reset the duty cycle
            if(adc.read() < 3000): #if the blue button has been pressed
                faultflag = 0 #allow the motors to run again
                start = 300 #set the start back to 300 to give the motors time again
            else:
                pass #otherwise, keep not running
    else:
        if onoff[1] > 0: #if the y axis was positive, pull the table down
            var.set_duty(90)
        else:
            var.set_duty(-90) #if the y axis was negative, push the table up
        start = 300
        
    

