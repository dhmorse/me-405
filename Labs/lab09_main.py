"""
@file lab09_main.py
@brief 		Balancing the platform
@details 	This script incorperates the drivers developed for the balancing 
			platform's motors, encoders, and resistive touch panel. A controller 
			uses the functions developed in the drivers to collect  potentiometer 
			and touch panel readings. These readings inform motor torque and duty 
			cycle calculations to level the platform and center the object placed 
			atop the tilt table.


@author Hunter Morse
@author Jacob Everest
@date March 11, 2021
"""

import pyb
import utime
import micropython

from pyb import Pin, Timer, ExtInt

from comboDriver import *
from motorDriver import *
from encoderDriver import *

def balance(controller):
	"""
	@brief		Balance
	@details 	This is the main loop run during operation. It contnuously 
				balances the motor at its initial encoder reading 
	@param controller Controller object for the full base assembly (Encoders + MotorBase)
	"""
	# Print encoder and motor values to verify expected
	print("motor1: %d\tmotor2: %d" 		%(controller.motor1.id, controller.motor2.id))
	print("encoder1: %d\tencoder2: %d" 	%(controller.encoder1.id, controller.encoder2.id))

	while(True):
		# Balance motor1 and motor2 at initial position (indicated by 0)
		# the initial position is the position of the platform at boot-up
		controller.goToPosition(controller.motor1, controller.encoder1, 0)
		controller.goToPosition(controller.motor2, controller.encoder2, 0)


def main():
	"""
	@brief 		Main
	@details	Here all pertenent variables and objects are initialized prior
				to begining the balance loop. This function is automatically 
				run once the system has booted
	"""
	motors = makeMotors()				# Default freq of 5000 Hz  -> tuple of 2 DCMotors
	base = makeMotorBase(motors)		# MotorBase object
	encoders = makeEncoders()			# Default prescaler of 7, period 0xff  -> tuple of 2 Encoders

	controller = makeController(base, encoders)		# Controller object made from base and encoders

	controller.enableMotors()			# Enable both DC motors (duty 0)
	balance(controller)					# Run balancing loop



if __name__ == '__main__':
	main()