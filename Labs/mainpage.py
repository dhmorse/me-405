'''
@file mainpage.py
@mainpage
@brief
@details
@section sec_intro Introduction
ME 405 - Mechatronics: This class covers microprocessor applications in 
machine control and product design. The ciriculum covers applied electronics, 
drive technology, transducers, electromechanical systems, real-time 
programming, and mechatronic design methodology. For Winter 2021 the class is taught 
virtually over zoom. The microprocessor used is the STM32 on a Nucleo-L476RG 
flashed with MicroPython.

@section sec_assignments Course Assignments
	* \ref pg_labs
	* \ref termProject.py

@author Hunter Morse
@date Winter 2021






@page pg_labs LABS
@brief The following is a complete list of labs covered prior to the the term
project. Click on a link to learn more and explore the code.

@section sec_labs Labs
	* \ref sec_lab01
	* \ref sec_lab02
	* \ref sec_lab03
	* \ref sec_lab04
	* \ref termProject.py
	


@section sec_lab01 Lab01: Reintroduction
@details 	The goal of lab01.py is to regain comfort with python 
			and the concepts learned in ME 305 by developing a program that 
			"embodies the function of a vending machine." The FSM diagram can be
			found by visiting the \ref pg_lab01 page.
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab01/)

	
@section sec_lab02 Lab02: Hardware Familiarization
			The goal of lab02.py is to gain familiarization with the Nucleo-L476RG
			board by building a game that incorperates buttons, LEDs, and one of the 
			built in timers. For more details and a screenshot of the gameplay, visit 
			the \ref pg_lab02 page.
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab02/)
	
@section sec_lab03 Lab03: Communication via Serial
			The goal of lab03.py lab is to become comfortable with serial communication 
			between the board and the PC as well as gain experience with analog to 
			digital conversion, and using matplotlib. See the ADC button response
			test on the \ref pg_lab03 page.
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab03/)

@section sec_lab04 Lab04: Temperature Readings via I2C
			The goal of lab04.py is to gain familiarity with I2C communication by creating
			and testing a driver for the MCP9808 temperature sensor. For a graph of the
			data collected check out the \ref pg_lab04 page.

			This is lab was done with Jacob Everest-Wikler 
			(https://mediocre-code.bitbucket.io/)
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab04/)



@page pg_lab01 Lab01: Reintroduction
@brief		A reintroduction to Python and finite state machines.
@details 	The goal of lab01.py is to regain comfort with python 
			and the concepts learned in ME 305 by developing a program that 
			"embodies the function of a vending machine."
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab01/)

@section sec_lab01_FSM Finite State Machine Diagram
@details 	The following is a proposed finite state machine for a vending machine.
			It follows the central "master mind" style taught in ME 305 pre-Covid.
@image html lab01_fsm.png "Vending Machine FSM" width=750px

@section sec_lab01_Code Vending Machine FSM Code
@details 	lab01.py was designed around the FSM pictured above. Each task is 
			singularly focused and virtually non-blocking. 

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab01/)
@author Hunter Morse
@date Winter 2021



@page pg_lab02 Lab02: Hardware Familiarization
@brief		Finding my away around the Nucleo-L476RG
@details	The game of lab02.py incorperates an LED, button, and timer into a 
            reaction game. One begun the player has a random amount of time
            between 2 and 3 seconds before the onboard green LED is illuminated.
            Once lit, the user has one second to press the blue button on the board.
            Their reation time is recored and reported. If they fail to press the 
            button within the second their session expires and their stats are
            printed to the screen before the next session begins automatically. 
            The game continues indefinitely unitl the user presses ^C to quit. 
			
			(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab02/)

@section sec_lab02_Game	The Game
@details	The following image is a screen grab of the RXN game readout after playing
			a few rounds. Think you're faster? Give it a try!

@image html lab02_game.png "Reaction Game Gameplay" width=750px

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab02/)
@author Hunter Morse
@date Winter 2021




@page pg_lab03 Lab03: Communication via Serial
@brief		Intro to ADCs
@details	This lab consists of two parts, lab03_main.py which is run on the 
			Nucleo, and lab03_ui.py which acts as the user interface on the host
			computer. After a handshake establishes communication between the 
			computer and Nucleo, the green LED on the board lights up. This 
			indicates the user can press the blue botton. An onboard ADC reads 
			the voltage data of resulting from the button press. Pressing the 
			user button on the Nucleo (default HI) cuases the voltage to drop 
			before returning to its HI state. This script sends data collected f
			rom the button press as a CSV to a host computer over a UART Serial 
			conncetion. The host UI then uses Matplotlib to process and plot the
			data. 
			The following image was generated from the CSV file in Excel because 
			it was more presentable.

@image html lab03_btnResponse.png "Button returning to elevated state from press" width=750px

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab03/)
@author Hunter Morse
@date Winter 2021




@page pg_lab04 Lab04: Temperature Readings via I2C
@brief		Introduction to I2C
@details	In lab04.py the Nucleo collects data from its internal temperature
			sensor as well as from a connected MCP9808 temperature sensor 
			(lab04_mcp9808.py). Data is collected once every minute and written 
			to a .CSV file on the device. Once started the program will run until the user 
			presses ^C or the user specified runtime is met. After 12 hours all
    		data will be overwritten so that the data file doesn't overflow the 
    		chips limited onboard storage. Finally, the data collected by the 
    		board is ploted on a host machine using Matplotlib in lab04_ui.py.

    		(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab04/)

@section sec_lab04_tempsPlot Temperature Plot
@image html lab04_tempsPlot.png "Onboard (orange) and MCP9808 (blue) Termperatures Plotted wrt Time" width=750px

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab04/)
@author Hunter Morse
@date Winter 2021
'''