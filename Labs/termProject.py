'''
@page termProject.py TERM PROJECT
@brief 		ME 405 Term Project: Balance Table
@details 	The goal of this project is to develop the necssary models, drivers, 
			and system parameters required to balance and center a ball on a 
			2-axis, pivoting platform. Each step in developing the final 
			product is divided into its own lab.

@section sec_projParts Term Project Labs:
	* \ref pg_lab05
	* \ref pg_lab06
	* \ref pg_lab07
	* \ref pg_lab08
	* \ref pg_lab09

@author Hunter Morse
@date Winter 2021




@page pg_lab05 Lab05: Pivoting Platform Kinematic Model
@details	The goal of this lab is to develop the equations of motion for the
			pivoting platform and ball balancing system. The equations should 
			describe the necesary outputs of the motors in order to adjust the 
			tilt of the platform such that the ball is balanced in the center 
			of the table. This lab was done in collaboration with Jacob Everest
			(https://mediocre-code.bitbucket.io/)

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab05/
see also https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab5/)

@section sec_lab05_Work	Hand Calcs
@details The following are images of (some) of the work I did to find the 
			equations of motion for the system. It took many more pages to
			come up with the assumptions used...

@image html lab05_p1.png width=1000px
@image html lab05_p2.png width=1000px
@image html lab05_p3.png width=1000px
@image html lab05_p4.png width=1000px
@image html lab05_p5.png width=1000px
@image html lab05_p6.png "Lab 5 Hand-calcs" width=1000px

@author Hunter Morse
@date Winter 2021


@page pg_lab06 Lab06: Simulation or Reality?
@details The goal of this lab is to develop a simulation and test a controller
			for the simplified model of the pivoting platform using the 
			equations derived from Lab 05. The lab was completed in 
			collaboration with Jacob Everest.

Jacob's BitBucket: https://mediocre-code.bitbucket.io/

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab06/
 see also https://bitbucket.org/mediocre-code/me-405-lab/src/master/Lab6/)
@section sec_lab06_Sections Sections:
	* \ref sec_lab06_Plots
	* \ref sec_lab06_Matlab

@section sec_lab06_Plots Scenarios
@image html lab06_scnA.png "Scenario A" width = 750
			The ball is placed in the center of already balanced platform. In 
			this scenario would not expect any movement to occur. Without any 
			external stimulus the ball and platform remain unperturbed. The 
			resulting graphs depicting postion, angle, linear velocity, and 
			angular velocity therefore make sense because they indicated a 
			constant, zero response with respect to time. 

@image html lab06_scnB.png "Scenario B" width = 750
			When the ball is placed 5cm off the center of a level platform,
			the response graphs indicate a dramatic change in the angle of 
			the platform and positon of the ball. Unexpectidly, neither 
			the velocity or angular velocity graphs are affected. This 
			indicates an error in the model concerning dynamic behavior.

@image html lab06_scnC.png "Scenario C" width = 750
			In this scenario the ball is placed above the center of gravity of
			the platform which lists at five degrees. The plots indicated a 
			response similar to that of the last scenario. The lack of change
			in velocities is concerning. At thhis point Jacob and I decided to 
			move forward with his results.

@image html lab06_scnD.png "Scenario D" width = 750
			Here the ball is placed above the center of gravit once again. This
			time the platform is level, however, there is a motor pulse of 1mN-m-s.
			The plots once again show a change in angle and position, but not 
			either velocity. It makes even less since in this scenario that the is 
			no velocity because an impuls immediately accelerates the system. 

@image html lab06_CL.png "Closed Loop" width = 750
			Based on the previous results I have no expectation for this being correct. 
			In fact the graph does not indicate the expected hysteresis from a 
			closed loop, untuned system. 

@section sec_lab06_Matlab MATLAB Results
@image html lab06_p1.png width=1000px
@image html lab06_p2.png width=1000px
@image html lab06_p3.png width=1000px
@image html lab06_p4.png width=1000px
@image html lab06_p5.png width=1000px
@image html lab06_p6.png width=1000px
@image html lab06_p7.png width=1000px
@image html lab06_p8.png width=1000px
@image html lab06_p9.png "Lab 6 MATLAB Simulation" width=1000px


@author Hunter Morse
@date Winter 2021






@page pg_lab07 Lab07: Feeling Touchy
@brief		Touchpanel Driver Development
@details	The goal of this lab is to develop a hardware driver to interface 
			the resistive touch panel with the STM32 microcontroller. The 
			final product is touchDriver.py.

(Source: https://bitbucket.org/dhmorse/me-405/src/master/Lab07/)

@section sec_l7hardware Hardware Setup
@details	I disassebled the platform from the body of the machine to make it
			easier to work with the board and table simultaneously. Beginning 
			this project I made a soldering mistake. After desoldering the FPC 
			adapter from the main assembly the through holes were blocked. 
			Despite spending hours attempting to remove the blocking solder 
			and eventully recruiting a CPE friend for assistance I was 
			unsuccessful and fried the board (a new one is coming in the mail). 
			Thankfully, a classmate was generous enough to let me borrow his! 
			The following is my set-up for testing and results from calibration:

@image html lab07_setup_new.png "RTP Testing Set-up" width=1000px

@section sec_l7results Results:
Calibration yeilded the following results:
	Bottom Left : (0380, 3800)
	Bottom Right: (3600, 3800)
	Top Left : (0420, 0220)
	Top Right: (3620, 0220)
	Center: (2020, 2020)

@image html lab07_calibration.png "RTP Calibration Results" width=1000px
During testing x-scan and y-scan speeds were measuered to be 300us 
			+/- 5. Total scan time took less than 1000us at roughly 975us for 
			x, y, and z scanning. Scan times were measured using utime.ticks_us 
			with extraneous print statements commented out. See below for output 
			results:

@image html lab07_results.png "RTP Scan Time Results" width=1000px

Results are formated:
	[xscan result] [yscan result] [zscan result] [total op. time].

@author Hunter Morse
@date Winter 2021







@page pg_lab08 Lab08: Motor and Encoder Drivers
@brief 		Motor and Encoder Drivers Development
@details	The goal of this lab was to develop drivers for the DRV8847 motor 
			driver chip and attached 16-bit encoder. The final results are 
			motorDriver.py and encoderDriver.py.

This lab was done in collaboration with Jacob Everest 
(https://mediocre-code.bitbucket.io/)

(Source: https://bitbucket.org/dhmorse/mechatronics_shared/src/master/Lab08/)

@section sec_lab08_Encoder Encoder
@details	The timer used for the encoder has a 0xff period and prescaler of 8. 
			At these settings the encoder resolution is 256 counts per rotation. 
			Positive rotation is defined to be counter clockwise and negative is 
			clockwise. Once reaching a count of 255 the encoder loops back to 0 
			and continues. Similarly, rolling through 0 clockwise will 
			loop to 255 and proceded to decreasing values. 

@section sec_lab08_Motordriver Motor Driver and Base
@details 	The DC motors of the pivoting platform can be enabled/disabed by 
			setting their respective nSleep pins. In addition to an nSleep pin,
			the motors have an nFault pin. This function pin is triggered when 
			the current passing through the motor driver is too high. To better 
			understand the nFault bit a USB logic analyzer was used to measure 
			the signal from the nFault pin. The following image is a screenshot 
			of the results. 

@image html lab08_nFaultTesting50-50_bothHeld.png "nFault Pin Testing" width=1000px

Because of the hysteresis, the motor cannot rely on the nFault bit 
			stopping motor operation. An external interrupt is therefore required 
			to trigger a stop condition that can only be overwritten manually. 
			This improves overload safety for the motors as well as improved 
			operator safety. While develping the program we had an issue with the
			nFault bit triggering on motor start-up. The current code works 
			around the issue with an additional check and set condition. 
			Additionally, because the nFault pin cannot be set up for each motor 
			individually, a MotorBase object is required. This object requires two
			Motor objects and handles setting the nFault pin, and handling the 
			resulting ISR.
@author Hunter Morse
@date Winter 2021








@page pg_lab09 Lab09: All Together Now
@brief 		ME 405 Term Project Submission

@image html lab09_platform.png "Pivoting Platform" width=500px

@details	In this lab we finished development of our term project. The 
			objective of the assignment was to balance a 2cm diameter 
			rubber mouse ball on a two axis pivoting platform. The assembly 
			consisted of the tilting platform, a resistive touch panel
			(ER-TP080-1) atop the platform, and two DC motors (controlled by a
			DRV8847 driver) on the assembly's base. The system was controlled 
			by a Nucleo-L476RG flashed with MicroPython and connected to a 
			custom PCB boot designed by Charlie Refvem of Cal Poly's 
			Mechanical Engineering Department. The documentation for this lab 
			is divided into the following sections:

@section sec_lab09_Contents Contents:
	* \ref sec_lab09_HowItWorks
	* \ref sec_lab09_Results
	* \ref sec_lab09_Gainstuned 
	* \ref sec_lab09_Attempts
	* \ref sec_lab09_Discussion
	* \ref sec_lab09_Calculations
	* \ref sec_lab09_Links

@section sec_lab09_HowItWorks How it Works
@details	The main lab file lab09_main.py successfully balances the platform.
			To operated it must be flashed onto the nucleo alogside its several.
			These files include: comboDriver.py, encoderDriver.py, 
			motorDriver.py, and touchDriver.py. Additionally, the program uses 
			the micropython, pyb, and utime libraries. The source code can be 
			found on BitBucket (see \ref sec_lab09_Links)

To balance the ball its position and velocity must be found in addition to the 
			angle and angular velocity of the platform. The position and linear 
			velocity are determined using the resistive touch panel. The 
			position is found by scanning the x and y-axes of the touch panel 
			using the driver developed in Lab07 (touchDriver.py) Velocity is 
			then determined by comparing the change in position of the ball 
			between two readings with respect to time. The angle and angular 
			position of the platform are measured using a similar method, but 
			with the encoders rather than touch panel. Future iterations of 
			this project will incorperate an IMU. Measurements from its 
			integrated accelerometer compared with the known, vertical 
			acceleration of gravity would provide the platform tilt angle. 
			Meanwhile, the sensor's gyroscope would provide angular 
			acceleration values. 

Once acquired, measured values are used to find the motor torques required to
			balance the ball. Torques are determined by multiplying the 
			measured values by calculated motor gains. The torques for each 
			motor are then converted to a duty cycle value which is used to 
			move the motors appropriately. The calculations for the gains can 
			be found in \ref sec_lab09_Calculations.

More detail on the methods used: The best results we were able to achieve 
			determined the angle and angular acceleration of the table from 
			the encoder position. We did this by dividing the position (and 
			speed) by 255 (the total counts in a full rotation). This poduced 
			the the motor's location a percentage of its total rotation. By 
			multiplying that percentage by 2pi, we determined the displacement
			of the motor (and in effect the platform) in radians. From there we 
			could multiply the length of the motor arm by the sine of its angle
			to get the vertical displacement at the end of the rod. That 
			displacement was also the change in height of the edge of the table 
			because we assume that the push rod stayed vertical. From there 
			taking the arcsine of that vertical displacement and dividing by the 
			length from the center of the table to the edge, produced the angle 
			of the table. The same could be done with the change in encoder 
			position to get the change in table angle. The code then multiplies 
			each of these results by the gain value calculated. Because our 
			touchscreens did not work, we were unable to balance the ball on the 
			table as we had hoped. If we were able to do so, the process would
			be similar to that of the angles. With the touch screen implemented, 
			we would be able to easily tell the ball's position. By consistently 
			interpreting values scanned by the screen, could obtain the change in 
			position with respect to time, giving us the velocity of the ball. 
			As touched on earlier, we could then multiply those by the gains we 
			computed. Once complete, we could sum up all of those torques, from 
			the angle, position, angular velocity, and velocity, multiply by the 
			motor resistance and 100%, then divide by the power supply voltage 
			(12V) and the torque constant (0.0138 Nm/Amp) to get the necessary 
			duty cycle. As it is, we could only compute the duty cycle without 
			the torques produced from the linear speed and position of the ball. 

@section sec_lab09_Results Results
https://youtu.be/_cNrHkeaciU

https://youtu.be/JoiuYJn16t4

We were unable to successfully balance the ball on the platform. We attempted the 
			problem from a number of angles with varying methods 
			(see \ref sec_lab09_Attempts). 

However, we are proud to say that we did end up figuring out how to apply the
			gain value method that we learned in class to our project. In a classic 
			"learn by doing" scenario we frankly didn't fully understand it 
			unitl we tried it... multiple times. As far as the gains go, we 
			learned that the ones we produced from hand calculations were far
			lower than what was required to properly contol the table. With 
			some tuning we were able to smoothen the leveling process. We think
			this had to do with the friction present in the physical system being
			much higher than that assumed in our calculations. We shot for a 
			settling time of 1 second and a percent overshoot of 1%. Aspirational 
			goals give the state of our system, we unsuprisingly were incapable of 
			achieving them. The greatest improvements we could make to the system
			to achieve the desired response would include tightening the belts and 
			reducing the friction between the belts and their gaurds.


@section sec_lab09_Gainstuned Tuned Gains
Original Gains: K = [-2.1474, 0.0072, -7.5486, -1.7775]

Tuned Gains: K = [NotApplicable, 1.3, NotApplicable, 8.2]


@section sec_lab09_Attempts The Attempts
We made a number of attempts to create a fully indtegrated system respondant to a
			touch panel. The following are the source files to a number of such 
			attempts. It is recommended to view them in the order they were 
			attempted as the doxygen descriptions each attempt references 
			previous versions. The fifth file is a cleaner version of Attempt 3 
			that offers greater modularity and can more easily be run on other 
			balancing systems. The following video captures one of the earlier
			balancing attempts: https://youtu.be/0fpSM2Fekm0
	* 1. mainWithnFault.py
	* 2. mainWithTouchPad.py
	* 3. mainThatBalancedtheTable.py (<- the one that worked!)
	* 4. BalancingFunctionalityAttempt.py
	* 5. lab09_main.py (Actual main-- works + clean)



@section sec_lab09_Discussion Discussion
Although unsuccessful in fully achieving our goal by the project deadline, 
			we made significant progress overall and gained a much deeper 
			understanding of Micropython, motor/sensor interfacing, 
			modeling dynamic systems, and designing controllers. Our 
			biggest take away: this sh*t is time consuming and hard. 
			Nevertheless it is fascinating and endlessly engaging, hence 
			why we keep coming back for more.

A major hurtle we encountered which set us behind significantly was not having
			reliable readings from the touch panel. Both my partner and I had 
			issues. We each used the code we independently developed in Lab07. 
			Although we were both found success reading values previously, we
			did not have the same success in this lab. Regularly the touch 
			panel would incorrectly report being touched and/or provided bogus 
			readings. We believe the issued stemmed from the way we attached 
			our boards to our respective top plates. Lab07 tested the touch
			panel prior to adhering it to the top plate. Whether it be an error
			from soldering or adhesion we are unsure. A lot of time was 
			dedicated to diagnosing and resolving the issue before we decided
			to focus our efforts elsewhere. Despited not being implemented, 
			we developed code for the touch panel. It is untested, however.

On a brighter note we were successfull in balancing the platform and having it
			right itself when perturbed. Additionally we were able to 
			successfully calculate the required system gains (see 
			\ref sec_lab09_Calculations). And we learned a lot and had fun
			doing it! There's not much better than that.

Reflection:
If I had to restart the project from scratch, I would begin by incorporating 
			drivers separately thus minimizing the code to debug in each 
			iteration. Having everything included at once made it difficult 
			to find where the bugs were hiding. That was our main error with 
			mainWithnFault.py. Finishing this project on time was also 
			extremely difficult given my schedule during the tail end of the 
			quarter. Now that the quarter is over, I plan to spend Spring 
			Break debugging and completing the project.


@section sec_lab09_Calculations Gain Calculations
Calculations for the gains done mostly by hand, some parts in MATLAB

@image html lab09_GComp1.png width=1000px
@image html lab09_GComp2.png width=1000px
@image html lab09_GComp3.png width=1000px
@image html lab09_GComp4.png width=1000px
@image html lab09_GComp5.png "Gains Computations" width=1000px


@section sec_lab09_Links Links
@subsection ssec_lab09_videos Videos
Explanation: https://youtu.be/_cNrHkeaciU

Demo 1: https://youtu.be/JoiuYJn16t4

Demo 2: https://youtu.be/0fpSM2Fekm0 

@subsection ssec_lab09_source Source Code
Final: 		https://bitbucket.org/dhmorse/mechatronics_shared/src/master/Lab09/HM/

Attempts:	https://bitbucket.org/dhmorse/mechatronics_shared/src/master/Lab09/Ultimately%20Futile%20Work/


This lab was completed in collaboration with Jacob Everest 
https://mediocre-coder.bitbucket.io/ 



@author Hunter Morse
@date Winter 2021
'''