"""
@file lab02.py
Documentation of lab02.py

@author Hunter Morse
@date Jan 28, 2021
"""
from random import uniform as ru
import utime as ut
import micropython  # included for ease of debugging
import pyb


class RxnGame:
    """
    Store values and functions useful for keeping track of interupts
    """ 
    def __init__(self, LED_pin, BTN_pin, timer, limit):
        """
        @param LED_pin LED to light for the user
        @param BTN_pin button for user input
        @parm timer timer to keep track of time elapsed since press
        @param limit max number of secconds to keep the LED on 
        """
        self.LED = pyb.Pin(LED_pin, mode=pyb.Pin.OUT)
        self.BTN = pyb.Pin(BTN_pin, mode=pyb.Pin.IN)
        self.LED = LED_pin
        self.BTN = BTN_pin
        self.timer = timer
        self.time_limit = limit

        self.start_time = 0
        self.end_time = 0
        self.rxn_time = 0
        self.total_time = 0
        self.BTN_pressed = False
        self.isr_count = 0

        self.BTN = pyb.ExtInt(self.BTN, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.pressedBTN)

    def getAveRxnTime(self):
        """
        Calculate the average rxn time of the user over all attempts
        """
        ret = 0;
        if(self.isr_count > 0):
            ret = self.total_time/self.isr_count
        return ret

    def start(self):
        """
        Turns on the LED and starts a timer to count how many 
        micro-seconds it takes the user to press the button. 
        Note: the LED turns on and then a couple of instructions 
        are executed. 
        """
        self.BTN_pressed = False
        self.LED.off()
        ut.sleep_ms(round(ru(2, 3)*1000))       # Sleep for some time btwn 2 and 3 sec
        self.LED.on()
        self.timer.counter(0)                   # set timer to 0
        self.start_time = self.timer.counter()  # save start time

    def pressedBTN(self, home):
        """
        ISR called whenever the user button is pressed on the board
        @param home is auto-handled by the IRQ
        """
        self.end_time = self.timer.counter()    # save end time
        self.LED.off()                          # turn off LED
        self.isr_count += 1                     # inc isrCount
        self.rxn_time = (self.end_time - self.start_time)
        self.total_time += self.rxn_time
        self.BTN_pressed = True

    def resetStats(self):
        """
        Reset game statistics for a new round
        """
        self.start_time = 0
        self.end_time = 0
        self.rxn_time = 0
        self.total_time = 0
        self.BTN_pressed = True
        self.isr_count = 0

    def printStats(self):
        """
        Print reaction time statistics like average reaction time and total tries
        """
        print('Average reation time: %.03f seconds' %(self.getAveRxnTime()/1000000))
        print('Total tries: %d' %(self.isr_count))

def gameLoop(game):
    """
    Game continues play until ^C pressed.
    @param game is a RxnGame object
    """
    while(True):
        print("Round: %d" %game.isr_count)
        game.start()
        while(not game.BTN_pressed):
            # If the button hasn't been pressed yet keep checking
            if(game.timer.counter() > game.time_limit):
                # Time runs out print message, stats and reset
                print("Too slow!")
                game.printStats()
                game.resetStats()
        print("... %.03f sec" %(game.rxn_time/1000000))

def main():
    """
    Main
    """
    ## Allocate memory for ISR debug messages
    micropython.alloc_emergency_exception_buf(255)

    ## Green LED set as output. Blue button set as input
    g_LED = pyb.Pin.board.PA5   # pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT)
    b_BTN = pyb.Pin.board.PC13  # pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

    ## Est the 32 bit counter, Timer 2, as t2
    t2 = pyb.Timer(2, prescaler = 79, period = 0x7fffffff)
    
    one_sec = 1000000       # one second is 1e7us
    
    ## Create Tstore object, store
    game = RxnGame(g_LED, b_BTN, t2, one_sec)

    try:
        gameLoop(game)

    except KeyboardInterrupt:
        # If ^C pressed stop the program
        print('Leaving so soon?')

    
    finally:
        # Upon stop, display the ave rxn time over num tries
        game.printStats()
        game.resetStats()  


if __name__ == '__main__':
    main()
