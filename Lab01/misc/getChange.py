"""
@file getChange.py
Documentaion for use of getChange.py

@author Hunter Morse
@date Jan. 13, 2021
"""
def findTotal(payment):
	"""
	Finds total cent value of all change
	accept: tuple of bills/coins
	return: total amount in cents
	"""
	total = 0
	
	if(len(payment) != 8):
		print("Payment tuple is not complete")
		return -1

	total += payment[0]*1
	total += payment[1]*5
	total += payment[2]*10
	total += payment[3]*25
	total += payment[4]*100
	total += payment[5]*500
	total += payment[6]*1000
	total += payment[7]*2000

	if(total < 0):
		return -1

	return total

def findCoins(change):
	"""
	Find change in fewest units
	accept: int to convent to as few coins a possible
	return: list of denominations representing change
	"""
	denom = {2000:0, 1000:0, 500:0, 100:0, 25:0, 10:0, 5:0, 1:0}
	for key in denom.keys():
		denom[key] = change//key
		change %= key

	ret = list(denom.values())
	ret.reverse()
	return ret

def getChange(price, payment):
	"""
	Computes change 
	accept: price as int representing price in cents
			payment as tuple of all bills/coins used
	return: tuple of change to be returned
	"""
	total  = findTotal(payment)
	change = 0

	# check price
	if(isinstance(price,float)):
		print("Price should be represented as in cents as an int")
		return -1
	elif(price<0):
		print("Price error: negative value")
		return -1

	# check payment
	for denom in payment:
		if denom < 0:
			print("Payment error: negative denomination")
			return -1
	if(total<0):
		print("Payment error: negative value")
		return -1
	elif(total==0):
		print("Payment error: no payment provided")
		return -1
	elif(total<price):
		print("You're %d cents short" %(-(total-price)))
		return -1

	# payment is suffient
	change = total-price
	ret = findCoins(change)
	return tuple(ret)

	return 0

if __name__ == '__main__':
	pass





















