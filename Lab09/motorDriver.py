# -*- coding: utf-8 -*-
"""@file motorDriver.py
@brief Runs the Motor
@details Initializes a motor object when used by the user. This can be done with
         multiple different pin values in order to run more than one motor at
         a time. It uses timer channel objects and pulse width modulation to 
         run the motors connected to the right pins at varying speeds. These 
         pins can be attached to the same timer number, but must have different
         channels on that timer. Thus, they can run independently of each other.
@author Jacob Everest
@author Hunter Morse
@date Mar. 9, 2021
@copyright None
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

import micropython

class MotorBase:
    '''
    @brief Fault Section
    @details Sets up our fault pin ISR and saves motors with names then calls
             methods from the DCMotor Class to enable and disable them
    @author Hunter Morse
    @author Jacob Everest
    @date Mar. 9, 2021
    @copyright No
    '''
    def __init__(self, nFault_pin, motor1, motor2):
        '''
        @brief Initialization section
        @details Saves the parameters as self. variables in order to be used in
                 other parts of the code. Also sets the fault pin and enables
                 the external interrupts.
        @param nFault_pin fault detection pin 		[IN]
        @param motor1 DC motor 1
        @param motor2 DC motor 2
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        self.motor1 = motor1
        self.motor2 = motor2
        self.faultPin = Pin(nFault_pin, mode = Pin.IN)

        # Set fault pin to read fault voltages
    #	self.faultADC = pyb.ADC(nFault_pin)

        # Set fault pin to generate IRQs
        ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)

        # Motor list for iteration
        self.motors = [self.motor1, self.motor2]

    def faultISR(self):       
        '''
        @brief Fault pin ISR
        @details Interrupt service routine to handle cases where the nFAULT bit has been 
                 tripped. To resume operation the motor must be re-enabled
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''       
        print("Fault triggered")
        # Faults are not currently enabled then ignore them
        flag = 0
        for motor in self.motors:
            flag = flag or motor.startup_flag


        if(flag):
            print("... nada")
            return

        # Otherwise stop and diable the motors
        for motor in self.motors:
            motor.stop()
            motor.disable()

    def enableMotors(self):
        '''
        @brief Enable both motors method
        @details Calls on the enable method from the DCMotor class in order to
                 allow both motors to be turned on. 
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        for motor in self.motors:
            motor.enable()

#	def checkMotorStatus(self):
#		"""
#		Check if motors have completed start-up. 
#		Return: 0 if start-up complete or 1 if start-up incomplete for either motor
#		"""
#		ret = 0
#		for motor in self.motors:
#			ret || motor.startup_flag
#		return ret
        


class DCMotor:
    ''' 
    @brief Motor Driver Class
    @details This class implements a motor driver for the
             ME405 board. 
    @author Hunter Morse
    @author Jacob Everest
    @date Mar. 9, 2021
    @copyright No
    '''

    def __init__(self, idNum, nSleep_pin, in1_pin, in2_pin, timer, ch1, ch2, duty=0):
        ''' 
        @brief Initializes the Motor Driver class
        @details Creates a motor driver by initializing GPIO pins and turning
                 the motor off for safety. Stores values as self.variables in
                 order to be used in different parts of the class.
        @param idNum ID number of motor
        @param nSleep_pin A pyb.Pin object to use as the enable pin.          [OUT]
        @param in1_pin A pyb.Pin object to use as the input to half bridge 1. [OUT]
        @param in2_pin A pyb.Pin object to use as the input to half bridge 2. [OUT]
        @param timer A pyb.Timer object to use for PWM generation on
               IN1_pin and IN2_pin.
        @param ch1 channel for in1_pin
        @param ch2 channel for in2_pin
        @param duty default to 0. Min -100, Max 100
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        print("Initializing and diabling motor...")

        self.id = idNum
        
        # Enable nSleep pin
        self.enablePin = Pin(nSleep_pin, mode = Pin.OUT_PP)
        # self.faultPin = Pin(nFault_pin, mode = Pin.IN)

        # Set up motor pins and timer channels for PWM control
        self.mPin1 = in1_pin
        self.mPin2 = in2_pin
        self.ch1 = timer.channel(ch1, Timer.PWM, pin = self.mPin1)
        self.ch2 = timer.channel(ch2, Timer.PWM, pin = self.mPin2)

        # Duty cycle (-100 --> 100)
        self.duty = duty

        # Enable IRQ on faultPin 
        # self.faultPin = ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)
        # self.faultPin.irq(trigger = Pin.IRQ_RISING, handler = self.faultISR)

        # Start-up flag raised until initialization complete
        self.startup_flag = 1

        print("... motor initialized")
        self.disable()

    def __repr__(self):
        """
        Define how a DCMotor object is represented
        """
        rep = 'DC Motor: %d' %self.id

    def enable(self):
        ''' 
        @brief Enabling Method
        @details Sets the enable pin object to high in order to allow
                 all motors connected to it to run.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        self.enablePin.high()
        print("Motor%d enabled" %self.id)

    def disable(self):
        ''' 
        @brief Disabling method
        @details Sets the enable pin object to low to stop the all
                 motors connected to the pin (stop output to them at least,
                 they can still coast or be pushed)
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        
        self.enablePin.low()
        print("... motor diabled")

    #def faultISR(self, home):
    #	"""
    #	Interrupt service routine to handle cases where the nFAULT bit has been 
    #	tripped. To resume operation the motor must be re-enabled 
    #	"""
    #	print("Fault triggered")
    #	self.stop()
    #	self.disable()

    def setDuty(self, duty):
        ''' 
        @brief Duty input
        @details This method sets the duty cycle to be sent
                 to the motor to the given level. Positive values
                 cause effort in one direction, negative values
                 in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal
                    sent to the motor 
        @author Hunter Morse 
        @author Jacob Everest
        @date Mar. 9, 2021
        '''

        # Set start-up flag to indicate current spike is expected
        self.startup_flag = 1

        # Ensure |duty| <= 100
        if(duty != 0 and abs(duty) != 100):				# Ok if == 0 or 100
            duty = (duty//abs(duty)) * (abs(duty)%100)

        # Check to see if duty alrady achieved 
        if(duty == self.duty):
            print("Motor%d Currently running at %04d" %(self.id, duty))
            return
        else:
            # If new duty diff. then reset both channels and save new duty
            self.duty = duty
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
            print("Duty reset")

        # [+]duty for ch1, [-]duty for ch2
        if(duty > 0):
            self.ch1.pulse_width_percent(duty)
        elif(duty < 0):
            self.ch2.pulse_width_percent(duty * -1)	# convert [-]duty to [+]duty

        print("Duty set to %04d" %duty)

        # Wait 500us then reset the start-up flag to indicate the motors have settled
        # A quicker, more cooperative way to do this would envolve setting up an ADC
        # to check the motor pin has settled then set the appropriate flags, rather
        # than wait for an arbitrary amount of time
        
        # utime.sleep_us(50)
        # self.ch1.pulse_width_percent(duty)
        # self.ch2.pulse_width_percent(duty)
        print("Duty reset")
        # utime.sleep_us(50)

        self.startup_flag = 0

    def driveFWD(self):
        '''
        @brief Forward Drive method
        @details Set duty on ch1 to drive motor forward continuously
        @params None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        '''
        duty = 40
        self.setDuty(duty)
        print("Driving forward")

    def driveBKWD(self):
        '''
        @brief Backward Drive method
        @details Set duty on ch2 to drive motor backward continuously
        @params None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        '''
        duty = -50
        self.setDuty(duty)
        print("Driving backward")

    def brake(self):
        '''
        @brief Brake Method
        @details Set duty cycle on ch1 and ch2 to cause motor to attempt
                 to turn in both direction at the same rate. This causes
                 them to remain stationary.
        @params None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        '''
        duty = 50
        self.ch1.pulse_width_percent(duty)
        self.ch2.pulse_width_percent(duty)
        print("Braking")

    def stop(self):
        ''' 
        @brief Stop Method
        @details This method sets the duty cycle to be sent
                 to the motor as 90 in both directions, therefore keeping the 
                 motor exactly where it is.
        @param None
        @author Hunter Morse 
        @author Jacob Everest
        @date Mar. 9, 2021
        '''
        duty = 0
        self.ch1.pulse_width_percent(duty)
        self.ch2.pulse_width_percent(duty)
        print("Motors stopped")


def makeMotorBase(motors):
    '''
    @brief Function for making the motor base
    @details Quickly make a MotorBase object given a tuple of 2 motor objects
             (from makeMotors() function
    @param motors A tuple of 2 DCMotor objects
    @return base MotorBase object
    '''
    # Motor nFault pin
    nFault_pin = Pin.cpu.B2

    # Create MotorBase object
    base = MotorBase(nFault_pin, motors[0], motors[1])

    return base



def makeMotors(freq = 10000):
    '''
    @brief Motor Making function
    @details Quickly create to motow objects at a given frequncy (default 5000Hz)
    @param freq Optional motor timer frequency (default 5000 Hz)
    @return motors A tuple containing two motor objects
    '''
    
    # Motor nSleep pin
    nSleep_pin = Pin.cpu.A15

    # Motor 1
    m1in1_pin = Pin.cpu.B4
    m1in2_pin = Pin.cpu.B5
    m1ch1 = 1
    m1ch2 = 2

    # Motor 2 pins
    m2in1_pin = Pin.cpu.B0
    m2in2_pin = Pin.cpu.B1
    m2ch1 = 3
    m2ch2 = 4

    # Create Timer
    tim3 = Timer(3, freq=freq)

    # Create motor objects
    motor1 = DCMotor(1, nSleep_pin, m1in1_pin, m1in2_pin, tim3, m1ch1, m1ch2)
    motor2 = DCMotor(2, nSleep_pin, m2in1_pin, m2in2_pin, tim3, m2ch1, m2ch2)

    return (motor1, motor2)

"""
notes:
    nFAULT bit only occurs when both motors are held
    oscillating behavior
"""
