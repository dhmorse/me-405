# -*- coding: utf-8 -*-
"""@file encoderDriver.py
@brief Shows where the motor is
@details This class creates an interface between the Nucleo and the motors. The
         Encoder has a spoked wheel inside of it that spin with the same 
         angular velocity as the motor. It shines a light through that wheel 
         and two sensors "observe" that light shining through. These two
         sensors are out of phase by 90 degrees, which means that when we 
         xor the outputs of both sensors on the same timeline, we find the 
         speed of the spoked wheel and therefore the speed of the motor. This
         class allows the user to create an object that can be used to read 
         that position of the encoder (and therefore the motor). The individual
         reading is only useful relative to the particular angle the motor was
         at when it turned on, as that is the set zero point, but collectively
         multiple readings can be used to tell the speed accurately.
@author Jacob Everest
@author Hunter Morse
@date Mar. 9, 2021
@copyright Hunter might be fine with this word
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

import micropython

class Encoder:
    """
    @brief Tells where the motor is at any given time
    @details Input 2 pins, 2 channels, and a timer object to run. Once given,
             this object will be able to read the motor position, update its 
             position, set a desired position, compute its change in position,
             and compute how far it is from the users desired position. These
             position readings can be used to determine where the motor is at 
             any given time relative to where it started as well as the motor 
             speed.
    @autor Hunter Morse
    @author Jacob Everest
    @date Mar. 9, 2021
    @copyright 
    """

    def __init__(self, idNum, pin1, pin2, ch1, ch2, timer):
        """
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param idNum ID number of motor
        @param pin1 pin to read encoder chA values
        @param pin2 pin to read encoder chB values
        @param ch1 encoder channel A
        @param ch2 endcoder channel B
        @param timer timer to read encoder
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """

        self.id = idNum

        self.pin1 = pin1
        self.pin2 = pin2
        self.timer = timer
        self.timer.channel(ch1, Timer.ENC_A, pin = self.pin1)
        self.timer.channel(ch2, Timer.ENC_A, pin = self.pin2)
        
        self.prevPosition = 0
        self.position = 0

        self.maxVal = 255		# max number of ticks encoder to read before roll over
                                # must be updataed based on prescaler...

    def __repr__(self):
        """
        @brief Name it
        @details Define how an Encoder object is represented.
                 Basically we give it a name.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        rep = 'Encoder: %d' %self.id

    def update(self):
        """
        @brief Update method
        @details Updates the position of the motor so that the user can later
                 tell how far the motor has traveled. It stores the current
                 value stored in the position variable as "prevPosition", then 
                 runs the getPosision() method that will be described later to 
                 put the current value for the position into the position 
                 variable. This way the most recent position value and the one
                 before it are both stored somewhere. Does not return a value.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        self.prevPosition = self.position
        self.position = self.getPosition()

    def getPosition(self):
        """
        @brief Method to get the position
        @details This method uses the counter method of our timer object to 
                 count how many ticks have gone by since the last time we
                 passed our zero point on the encoder. That means that it 
                 simply tells us where we are in our encoder rotation. This 
                 method returns a numerical value which represents the motor 
                 position.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """
        return(self.timer.counter())

    def setPosition(self, pos = 0):
        """ 
        @brief Saves the users desired position
        @details The setPosition method allows the user to input a position
                 which this method will then save as a value to be used at a 
                 later time. This value will most likely be zero, as the user 
                 will want to start their table off in a balanced state, 
                 otherwise the user will have no idea how far the system needs 
                 to go in order to become level.
        @param pos An inputed value for the user, allowing them to choose 
                     where they want to be, default 0
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        self.position = pos

    def getDelta(self):
        """
        @brief Change in position
        @details Takes the two most recent values from the update method and 
                 computes the difference between the two. If the user were
                 timing the system and taking values at a specific interval,
                 this could be used to compute the speed of the motor. However,
                 the user must use the update method after this one in order to
                 change the most recent value and replace the prevPosition 
                 value with the position value, otherwise this method will 
                 return the same value over and over.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """
        return(self.prevPosition - self.position)

def makeEncoders(prescaler = 7, period= 0xff):
    """
    @brief Function to make encoders
    @details This function provides all the necessary data and a script to work from
             that will create and run the encoders as desired by us, the creators.
    @param prescaler Scales the time unit
    @param period Number of counts
    @return A tuple that contains both of the encoders
    """
    # Constants:
    # prescaler = 7 period = 0x7fffffff			# 0-255 for a full rotation but 0xffff total
    # Use the following for 0-255 both directions 
    # prescaler = 7
    # period = 0xff

    # Encoder 1:
    e1Pin1 = Pin.cpu.B6
    e1Pin2 = Pin.cpu.B7
    e1Ch1 = 1
    e1Ch2 = 2
    e1Tim = Timer(4, prescaler = prescaler, period = period)

    encoder1 = Encoder(1, e1Pin1, e1Pin2, e1Ch1, e1Ch2, e1Tim)

    # Encoder 2:
    e2Pin1 = Pin.cpu.C6
    e2Pin2 = Pin.cpu.C7
    e2Ch1 = 1
    e2Ch2 = 2
    e2Tim = Timer(8, prescaler = prescaler, period = period)
    
    encoder2 = Encoder(2, e2Pin1, e2Pin2, e2Ch1, e2Ch2, e2Tim)

    return(encoder1, encoder2)






"""
notes from testing:
    - with a prescalar of 7 and period of 0x7fffffff the encoder has a 
      resolution of 256count/360deg
"""
