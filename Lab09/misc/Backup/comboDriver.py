"""
@file comboDriver.py
@brief Lab 09
@details Combines the Encoder and Motor Drivers to successfully move motors
		 to a desired position

@author Hunter Morse
@date March 11, 2021
"""
import pyb
import utime
import micropython

from pyb import Pin, Timer, ExtInt
from motorDriver import MotorBase, DCMotor
from encoderDriver import Encoder


class Controller:
	"""
	Incorperates motor and encoder driver to create a position cotrolled motor
	"""
	def __init__(self, motorUnit, encoders):
		"""
		@param motorUnit A MotorBase object consisting of 2 DC motors
		@param encoders A list of encoders associated with the the DC motors on the MotorBase
		"""
		self.motorBase = motorUnit

		self.motor1 = motorUnit.motor1		# Motor objects in the MotorBase class
		self.motor2 = motorUnit.motor2

		self.encoder1 = encoders[0]			# Encoder objects in the encoders list
		self.encoder2 = encoders[1]

		self.max = self.encoder1.maxVal		# Max number of ticks read by encoder

	def goToPosition(self, motor, encoder, position):
		"""
		Sets appropriate motor duty to achieve desired postion
		@param motor The motor to move to a desired position
		@param encoder The encoder associated wtih the motor 
		@param position The desired position for the motor
		"""
		# Note: CCW : positive, CW: Negative

		# Set desired encoder position
		encoder.setPosition(position)
		# Update encoder
		encoder.update()
		# Find difference between desired and current positions
		delta = encoder.getDelta()

		# Find #ticks and direction to move
		# if delta > halfway point go otherway
		if(abs(delta) >= 128):
			delta = self.max + delta

		# Set motor duty cycle (min -100, max 100)
		# Duty cycle = R*Tm/(K*V) where R is the motor resistance (2.21), V is 12, Kt = 0.0138 Nm/Amp
		#
		if(abs(delta) > 70):
			duty = 90*(delta//abs(delta))	# want power of 70 in directoin of delta
		elif(10 < abs(delta) and abs(delta) < 40):
			duty = 50*(delta//abs(delta))	# want power of 40 in directoin of delta
		elif(0 < abs(delta) and abs(delta) <= 10):
			duty = 30*(delta//abs(delta))	# want power of 20 in directoin of delta
		
		else:
			duty = delta%100	

		# Check Vals:
		print("Motor%d:\tcurPos: %d\tdesPos: %d\tdelta: %d\tduty: %d" \
			%(motor.id, encoder.position, encoder.prevPosition,delta, duty))

		motor.setDuty(duty)
		

	def enableMotors(self):
		"""
		Enable both motors of the MotorBase
		"""
		self.motorBase.enableMotors()


def makeController(motorBase, encoders):
	"""
	Make a controller from a motor base and encoders
	@param motorBase MotorBase object consisting of 2 Motors
	@param encoders Tuple containing two Encoder objects
	@return controller Controller object created from input MotorBase and Encoders
	"""
	# Make controller
	controller = Controller(motorBase, encoders)

	return controller




		






