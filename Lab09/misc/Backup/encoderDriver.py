"""
@file encoderDriver.py
@brief Encoder Driver
@details Driver to interface the DC motor encoders with the STM32

@author Hunter Morse
@date March 11, 2021
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

import micropython

class Encoder:
	"""
	"""

	def __init__(self, idNum, pin1, pin2, ch1, ch2, timer):
		"""
		@param idNum ID number of motor
		@param pin1 pin to read encoder chA values
		@param pin2 pin to read encoder chB values
		@param ch1 encoder channel A
		@param ch2 endcoder channel B
		@param timer timer to read encoder
		"""

		self.id = idNum

		self.pin1 = pin1
		self.pin2 = pin2
		self.timer = timer
		self.timer.channel(ch1, Timer.ENC_A, pin = self.pin1)
		self.timer.channel(ch2, Timer.ENC_A, pin = self.pin2)
		
		self.prevPosition = 0
		self.position = 0

		self.maxVal = 255		# max number of ticks encoder to read before roll over
								# must be updataed based on prescaler...

	def __repr__(self):
		"""
		Define how an Encoder object is represented
		"""
		rep = 'Encoder: %d' %self.id

	def update(self):
		"""
		Update the position of the encoder
		"""
		self.prevPosition = self.position
		self.position = self.getPosition()

	def getPosition(self):
		"""
		Return the value of the encoder's current position
		"""
		return(self.timer.counter())

	def setPosition(self, pos = 0):
		"""
		Set position to a specified value. Default 0
		"""
		self.position = pos

	def getDelta(self):
		"""
		Return the difference between the previous and new encoder positions
		"""
		return(self.prevPosition - self.position)

def makeEncoders(prescaler = 7, period= 0xff):
	"""
	"""
	# Constants:
	# prescaler = 7 period = 0x7fffffff			# 0-255 for a full rotation but 0xffff total
	# Use the following for 0-255 both directions 
	# prescaler = 7
	# period = 0xff

	# Encoder 1:
	e1Pin1 = Pin.cpu.B6
	e1Pin2 = Pin.cpu.B7
	e1Ch1 = 1
	e1Ch2 = 2
	e1Tim = Timer(4, prescaler = prescaler, period = period)

	encoder1 = Encoder(1, e1Pin1, e1Pin2, e1Ch1, e1Ch2, e1Tim)

	# Encoder 2:
	e2Pin1 = Pin.cpu.C6
	e2Pin2 = Pin.cpu.C7
	e2Ch1 = 1
	e2Ch2 = 2
	e2Tim = Timer(8, prescaler = prescaler, period = period)
	
	encoder2 = Encoder(2, e2Pin1, e2Pin2, e2Ch1, e2Ch2, e2Tim)

	return(encoder1, encoder2)






"""
notes from testing:
	- with a prescalar of 7 and period of 0x7fffffff the encoder has a 
	  resolution of 256count/360deg
"""