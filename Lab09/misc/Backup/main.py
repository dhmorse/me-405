"""
@file main.py
@brief Lab 09
@details Testing the comboDriver

@author Hunter Morse
@date March 11, 2021
"""

import pyb
import utime
import micropython

from pyb import Pin, Timer, ExtInt

from comboDriver import *
from motorDriver import *
from touchDriver import *
from encoderDriver import *

def balance(controller):
	"""
	Contnuously balances the motor at its initial encoder reading
	@param controller Controller object for the full base assembly (Encoders + MotorBase)
	"""
	# Print encoder and motor values to verify expected
	print("motor1: %d\tmotor2: %d" 		%(controller.motor1.id, controller.motor2.id))
	print("encoder1: %d\tencoder2: %d" 	%(controller.encoder1.id, controller.encoder2.id))

	while(True):
		# Balance motor1 and motor2 at initial position
		controller.goToPosition(controller.motor1, controller.encoder1, 0)
		controller.goToPosition(controller.motor2, controller.encoder2, 0)

		#utime.sleep_us(50)


def main():
	"""
	Main fn to init all pertenent variable and objects and being main loop
	"""
	motors = makeMotors()				# Default freq of 5000 Hz  -> tuple of 2 DCMotors
	base = makeMotorBase(motors)		# MotorBase object
	encoders = makeEncoders()			# Default prescaler of 7, period 0xff  -> tuple of 2 Encoders

	controller = makeController(base, encoders)		# Controller object made from base and encoders

	controller.enableMotors()
	balance(controller)					# Run balancing loop



if __name__ == '__main__':
	main()