"""
@file motorDriver.py
@brief Motor Driver
@details Driver to interface the DC motors with the STM32 using PWM
and the DRV8847 motor driver

@author Hunter Morse
@date March 11, 2021
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

import micropython

class MotorBase:
	"""
	Balance table motor base
	"""
	def __init__(self, nFault_pin, motor1, motor2):
		"""
		@param nFault_pin fault detection pin 		[IN]
		@param motors list of DC motor objects	
		"""
		self.motor1 = motor1
		self.motor2 = motor2
		self.faultPin = Pin(nFault_pin, mode = Pin.IN)

		# Set fault pin to read fault voltages
	#	self.faultADC = pyb.ADC(nFault_pin)

		# Set fault pin to generate IRQs
		ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)

		# Motor list for iteration
		self.motors = [self.motor1, self.motor2]

	def faultISR(self, home):
		"""
		Interrupt service routine to handle cases where the nFAULT bit has been 
		tripped. To resume operation the motor must be re-enabled 
		"""
		print("Fault triggered")
		# Faults are not currently enabled then ignore them
		flag = 0
		for motor in self.motors:
			flag = flag or motor.startup_flag


		if(flag):
			print("... nada")
			return

		# Otherwise stop and diable the motors
		for motor in self.motors:
			motor.stop()
			motor.disable()

	def enableMotors(self):
		"""
		Enable both DC motors
		"""
		for motor in self.motors:
			motor.enable()

#	def checkMotorStatus(self):
#		"""
#		Check if motors have completed start-up. 
#		Return: 0 if start-up complete or 1 if start-up incomplete for either motor
#		"""
#		ret = 0
#		for motor in self.motors:
#			ret || motor.startup_flag
#		return ret
		


class DCMotor:
	"""
	DC Motor driver class
	"""

	def __init__(self, idNum, nSleep_pin, in1_pin, in2_pin, timer, ch1, ch2, duty=0):
		"""
		@param idNum ID number of motor
		@param nSleep_pin enable pin 				[OUT]
		@param nFault_pin fault detection pin 		[IN]
		@param in1_pin motor input pin 1 			[OUT]
		@param in2_pin motor input pin 2			[OUT]
		@param timer timer used for PWM generation	
		@param PWM ch1 channel for in1_pin
		@param PWM ch2 channel for in2_pin
		@param duty default to 0. Min -100, Max 100
		"""
		print("Initializing and diabling motor...")

		self.id = idNum
		
		# Enable nSleep pin
		self.enablePin = Pin(nSleep_pin, mode = Pin.OUT_PP)
		# self.faultPin = Pin(nFault_pin, mode = Pin.IN)

		# Set up motor pins and timer channels for PWM control
		self.mPin1 = in1_pin
		self.mPin2 = in2_pin
		self.ch1 = timer.channel(ch1, Timer.PWM, pin = self.mPin1)
		self.ch2 = timer.channel(ch2, Timer.PWM, pin = self.mPin2)

		# Duty cycle (-100 --> 100)
		self.duty = duty

		# Enable IRQ on faultPin 
		# self.faultPin = ExtInt(self.faultPin, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.faultISR)
		# self.faultPin.irq(trigger = Pin.IRQ_RISING, handler = self.faultISR)

		# Start-up flag raised until initialization complete
		self.startup_flag = 1

		print("... motor initialized")
		self.disable()

	def __repr__(self):
		"""
		Define how a DCMotor object is represented
		"""
		rep = 'DC Motor: %d' %self.id

	def enable(self):
		"""
		Set enablePin (nSleep) to HI to enable motors
		"""
		self.enablePin.high()
		print("Motor%d enabled" %self.id)

	def disable(self):
		"""
		Set enablePin (nSleep) to LO to disable motors
		"""
		self.enablePin.low()
		print("... motor diabled")

	#def faultISR(self, home):
	#	"""
	#	Interrupt service routine to handle cases where the nFAULT bit has been 
	#	tripped. To resume operation the motor must be re-enabled 
	#	"""
	#	print("Fault triggered")
	#	self.stop()
	#	self.disable()

	def setDuty(self, duty):
		"""
		Set duty cycle of the motor. Positive values increase effort in one
		direction, negative increase effort in the other.
		"""

		# Set start-up flag to indicate current spike is expected
		self.startup_flag = 1

		# Ensure |duty| <= 100
		if(duty != 0 and abs(duty) != 100):				# Ok if == 0 or 100
			duty = (duty//abs(duty)) * (abs(duty)%100)

		# Check to see if duty alrady achieved 
		if(duty == self.duty):
			print("Motor%d Currently running at %04d" %(self.id, duty))
			return
		else:
			# If new duty diff. then reset both channels and save new duty
			self.duty = duty
			self.ch1.pulse_width_percent(0)
			self.ch2.pulse_width_percent(0)
			print("Duty reset")

		# [+]duty for ch1, [-]duty for ch2
		if(duty > 0):
			self.ch1.pulse_width_percent(duty)
		elif(duty < 0):
			self.ch2.pulse_width_percent(duty * -1)	# convert [-]duty to [+]duty

		print("Duty set to %04d" %duty)

		# Wait 500us then reset the start-up flag to indicate the motors have settled
		# A quicker, more cooperative way to do this would envolve setting up an ADC
		# to check the motor pin has settled then set the appropriate flags, rather
		# than wait for an arbitrary amount of time
		
		# utime.sleep_us(50)
		# self.ch1.pulse_width_percent(duty)
		# self.ch2.pulse_width_percent(duty)
		print("Duty reset")
		# utime.sleep_us(50)

		self.startup_flag = 0

	def driveFWD(self):
		"""
		Set duty on ch1 to drive motor forward
		"""
		duty = 40
		self.setDuty(duty)
		print("Driving forward")

	def driveBKWD(self):
		"""
		Set duty on ch2 to drive motor forward
		"""
		duty = -50
		self.setDuty(duty)
		print("Driving backward")

	def brake(self):
		"""
		Set duty on both motor channels to stop the motor
		"""
		duty = 50
		self.ch1.pulse_width_percent(duty)
		self.ch2.pulse_width_percent(duty)
		print("Braking")

	def stop(self):
		"""
		Set duty on both motor channels to 0 so motor can coast
		"""
		duty = 0
		self.ch1.pulse_width_percent(duty)
		self.ch2.pulse_width_percent(duty)
		print("Motors stopped")


def makeMotorBase(motors):
	"""
	Quickly make a MotorBase object given a tuple of 2 motor objects
	@params motors A tuple of 2 DCMotor objects
	@return base MotorBase object
	"""
	# Motor nFault pin
	nFault_pin = Pin.cpu.B2

	# Create MotorBase object
	base = MotorBase(nFault_pin, motors[0], motors[1])

	return base



def makeMotors(freq = 10000):
	"""
	Quickly make two motor objects given a frequency (default 5000Hz)
	@param freq Optional motor timer frequency (default 5000 Hz)
	@return motors A tuple containing two motor objects
	"""
	# Motor nSleep pin
	nSleep_pin = Pin.cpu.A15

	# Motor 1
	m1in1_pin = Pin.cpu.B4
	m1in2_pin = Pin.cpu.B5
	m1ch1 = 1
	m1ch2 = 2

	# Motor 2 pins
	m2in1_pin = Pin.cpu.B0
	m2in2_pin = Pin.cpu.B1
	m2ch1 = 3
	m2ch2 = 4

	# Create Timer
	tim3 = Timer(3, freq=freq)

	# Create motor objects
	motor1 = DCMotor(1, nSleep_pin, m1in1_pin, m1in2_pin, tim3, m1ch1, m1ch2)
	motor2 = DCMotor(2, nSleep_pin, m2in1_pin, m2in2_pin, tim3, m2ch1, m2ch2)

	return (motor1, motor2)


#def main():
	"""
	"""
	# Constants:
	# freq = 5000		# [Hz]

	# # Fn pin objects
	# nSleep_pin = Pin.cpu.A15
	# nFault_pin = Pin.cpu.B2
	
	# # Motor 1
	# m1in1_pin = Pin.cpu.B4
	# m1in2_pin = Pin.cpu.B5
	# m1ch1 = 1
	# m1ch2 = 2

	# # Motor 2 pins
	# m2in1_pin = Pin.cpu.B0
	# m2in2_pin = Pin.cpu.B1
	# m2ch1 = 3
	# m2ch2 = 4

	# # Create Timer
	# tim3 = Timer(3, freq=freq)

	# # Create motor objects
	# motor1 = DCMotor(nSleep_pin, m1in1_pin, m1in2_pin, tim3, m1ch1, m1ch2)
	# motor2 = DCMotor(nSleep_pin, m2in1_pin, m2in2_pin, tim3, m2ch1, m2ch2)

	# base = MotorBase(nFault_pin, motor1, motor2)

# 	motor1.enable()
# 	motor2.enable()
	
# 	motor1.driveFWD()
# 	motor2.driveFWD()

#  	time = 10000000
#  	for sec in range(time):
# 	#	print("ADC val: %d" %(base.faultADC.read()))
# 		utime.sleep_us(1)
	
# 	print("out")	
# 	motor1.disable()
# 	motor2.disable()



# if __name__ == '__main__':
# 	main()


"""
notes:
	nFAULT bit only occurs when both motors are held
	oscillating behavior
"""