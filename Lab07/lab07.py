"""
@file lab07.py
@brief Feeling Touchy
@details Driver between touch display and STM32


@author Hunter Morse
@date March 4, 2021
"""

import pyb
from pyb import Pin
from pyb import ADC

import utime
from utime import sleep_us
from utime import ticks_us
from utime import ticks_diff

import micropython

class TouchPanel:
	"""
	@brief		Touch Panel Class
	@details	Class for a resistive touch panel. Used to hold panel parameters, 
				detect touches, and calculate touch locations
	"""

	def __init__(self, xp, xm, yp, ym, p_len, p_width, center, res = 0xfff, delay = 3):
		"""
		@param xp pin registered to xp
		@param xm pin registered to xm
		@param yp pin registered to yp
		@param ym pin registered to ym
		@param p_len panel length
		@param p_width panel width
		@param ADC center values (from testing)
		@param ox coordinate of panel origin (center)
		@param oy coordinate of panel origin (center)
		@param res resolution of adc
		@param delay read delay to ensure signal settled
		"""

		self.xp = xp
		self.xm = xm
		self.yp = yp
		self.ym = ym

		self.p_len = p_len
		self.p_width = p_width
		self.ox = center[0]
		self.oy = center[1]

		self.res = res
		self.delay = delay		# Read delay default 3. Can be adjusted based on testing

		self.hiPin = None
		self.loPin = None
		self.vPin = None		# Pin to read voltage
		self.fPin = None		# Pin to float


	def xScan(self):
		"""
		@brief 		Scan X axis
		@details	Determine touch's distance in mm from the center of the platform
		@return 	x the position of the touch from the center along x-axis [mm]
		"""
		#start_time = ticks_us()
		self.hiPin = Pin(self.xp, mode = Pin.OUT_PP).value(1)	# set xp HI
		self.loPin = Pin(self.xm, mode = Pin.OUT_PP).value(0)	# set xm LO
		self.vPin = ADC(self.ym)								# Read voltage from ym
		self.fPin = Pin(self.yp, mode = Pin.IN)					# Float yp

		sleep_us(self.delay)						# wait for signal to settle

		vout = self.vPin.read()						# read voltage from vPin
		x = (vout - self.ox)/self.res * self.p_width 

		
		#end_time = ticks_us()		
		#print("x.time = %d" %(ticks_diff(end_time, start_time)))
		#print("x.vout = %04d" %vout)

		return x * 1000		

	def yScan(self):
		"""
		@brief 		Scan Y axis
		@details	Determine touch's distance in mm from the center of the platform
		@return 	y the position of the touch from the center along y-axis [mm]
		"""
		
		self.hiPin = Pin(self.yp, mode = Pin.OUT_PP).value(1)	# set yp HI
		self.loPin = Pin(self.ym, mode = Pin.OUT_PP).value(0)	# set ym LO
		self.vPin = ADC(self.xm)								# Read voltage from xm
		self.fPin = Pin(self.xp, mode = Pin.IN)					# Float xp

		sleep_us(self.delay)						# wait for signal to settle

		vout = self.vPin.read()						# read voltage from vPin
		y = (vout - self.oy)/self.res * self.p_len
		
		#(vout/self.res)*self.p_width - self.oy	# get y position from center
		#print("y.vout = %04d" %vout)
		
		return y * 1000

	def zScan(self):
		"""
		@brief 		Scan Z axis
		@details	Detect touch/no touch based on x and y pin values
		@return boolean
		"""
		self.hiPin = Pin(self.yp, mode = Pin.OUT_PP).value(1)	# set yp HI
		self.loPin = Pin(self.xm, mode = Pin.OUT_PP).value(0)	# set xm LO
		self.vPin = ADC(self.ym)								# Read voltage from ym
		self.fPin = Pin(self.xp, mode = Pin.IN)					# Float xp

		sleep_us(self.delay)						# wait for signal to settle

		vout = self.vPin.read()						# read voltage from vPin

		if (vout < 0xf0f):							# check not HI
		#	print("Touch vout: %04d" %vout)
			return True								# panel touched
		
		#print("No touch vout: %04d" %vout)
		return False								# panel not touched

	def scan(self):
		"""
		@brief 		Scan X,Y,Z
		@details	Compile x, y, z scan values as tuple
		@return 	tuple
		"""
		return (self.xScan(), self.yScan(), self.zScan())

	def filter(self):
		"""
		@brief 		Signal filter
		@details	Filter noise from input signal. For implementation in future
		"""
		pass

def mainLoop(panel):
	"""
	@breif 		Main Loop
	@details	Print debugging info to screen
	"""
	while(True):
		results = panel.scan()

		if(not results[2]):
			# Screen not touched
			print("No touch")
		else:
			# Screen touched
			start_time = ticks_us()
			panel.scan()
			end_time = ticks_us()
			print("x: %04dmm   y: %04dmm   touched: %s   time: %05d" %(results[0], results[1], results[2], end_time-start_time))

		sleep_us(1000000)


def main():
	"""
	@brief 		Main
	@details	Init critical values and start mainLoop
	"""
	# allocate buffer for debug messages
	micropython.alloc_emergency_exception_buf(100)

	ym = Pin.cpu.A0
	xm = Pin.cpu.A1
	yp = Pin.cpu.A6
	xp = Pin.cpu.A7

	p_len = 0.185		# [m]
	p_width = 0.105		# [m]

	center = (2000, 2000)

	res = 0xfff

	# ox = p_width/2
	# oy = p_len/2

	panel = TouchPanel(xm, xp, ym, yp, p_len, p_width, center)

	mainLoop(panel)


if __name__ == '__main__':
	main()



"""
Bottom	Left : (0380, 3800)
	  	Right: (3600, 3800)
Top 	Left : (0420, 0220)
		Right: (3620, 0220)

Center: (2020, 20200)
""" 